use std::vec;
use std::num::max;

mod sdl;
mod williams;



/*
#[start]
fn start(argc: int, argv: **u8) -> int {
    std::rt::start_on_main_thread(argc, argv, main)
}
*/


fn color(bbgggrrr:u8) -> sdl::Color {
  let mut r = 0xE0 & (bbgggrrr << 5);
  let mut g = 0xE0 & (bbgggrrr << 2);
  let mut b = 0xC0 & bbgggrrr;
  r |= (r >> 3) | (r >> 6);
  g |= (g >> 3) | (g >> 6);
  b |= (b >> 2) | (b >> 4) | (b >> 6);
  sdl::Color { r:r, g:g, b:b, a:255 }
}

pub fn render(ram:&[u8], pixels:&mut[sdl::Color]) {
  let pal16 = vec::from_fn(16, |i| { color(ram[0xC000 + i]) });
  let put = |x:int, y:int, c:sdl::Color| {
    let d = 4 * (x + y * 304);
    pixels[d] = c;
  };
  for xy in range(0, 256 * 304 / 2) {
    let y = xy & 0xFF;
    let x0 = 2 * (xy >> 8);
    put(x0,   y, pal16[ram[xy] >> 4]);
    put(x0+1, y, pal16[ram[xy] & 0xF]);
  }
}


#[main]
#[fixed_stack_segment]
pub fn main() {

  let mut game = williams::Game::new(williams::defender);

  sdl::init();

  // std::set_title(game.name + if game.label.len() > 0 {" (" + game.label + ")"} else {""});

  let mut win = ~sdl::Window::create("Satan", 304, 256);
  let renderer = ~win.create_renderer();
  let texture = ~renderer.create_texture(304, 256);

  let mut pixels = vec::from_elem(304 * 256, sdl::Color { r:0, g:0, b:0, a:255 });

  let mut frame_count = 0;
  let mut paused = false;
  let mut last_frame = sdl::ticks();
  let msec_per_frame = 1000 / 60;

  let ESC = 27u8 as char;

  'main : loop {

    loop {
      match sdl::poll() {
        sdl::NoEvent => break,
        sdl::QuitEvent => break 'main,
        sdl::KeyEvent(down, _, key, _, _, _, false) =>
          if key == ESC {
            break 'main;
          } else if key == 'P' {
            paused = !paused;
          } else {
            game.handle_key(down, key)
          },
        _ => { }
      }
    }

    if !paused {
      game.run_frame();
      render(game.main.RAM, pixels);
      texture.update(pixels, 304);
      // renderer.clear();
      renderer.copy_texture(texture);
      renderer.present();
    }

    let elapsed = sdl::ticks() - last_frame;
    let remaining = msec_per_frame - elapsed;
    sdl::delay(max(remaining, 1));
    last_frame = sdl::ticks();
    frame_count += 1;
  }

  win.destroy();
  sdl::quit();
  println!("Bye. {} frames went by.", frame_count);
}
