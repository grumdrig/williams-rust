use std::io;
use std::path::posix::Path;
use std::hashmap;
use std::rand::random;
use std::num::min;
use std::vec;
use std::iter::range_step;
use std::ptr;

mod emu680X;
mod sdl;

type Cpu = emu680X::Cpu;




// There"s a bug in the blitter that xors bit 2 of the height and
// width. It"s fixed in later hardware, but not in any that I
// currently care about.
static m_blitter_xor:u8 = 0x4;

fn blit(mpu:&mut Cpu, start:u8, mask:u8, mut source:u16, mut dest:u16, mut width:u8, mut height:u8) {
  width ^= m_blitter_xor;
  height ^= m_blitter_xor;
  let sxstride: u16 = if start & 1 != 0 {256} else {1};
  let dxstride: u16 = if start & 2 != 0 {256} else {1};
  let transparency = start & 0x8 != 0;
  let solid = start & 0x10 != 0;
  let ror = start & 0x20 != 0;
  let even = start & 0x40 == 0;
  let odd = start & 0x80 == 0;

  for _y in range(0, height) {
    let mut s = source;
    let mut d = dest;
    let mut oldc = if ror {mpu.fetch((s + sxstride * (width as u16 - 1)) & 0xFFFF)} else {0};
    for _x in range(0, width) {
      let mut c = mpu.fetch(s);
      if (ror) {
        let newc = 0xFF & ((oldc << 4) | (c >> 4));
        oldc = c;
        c = newc;
      }
      if solid {
        c = if transparency {
              (if c & 0xF0 != 0 {mask & 0xF0} else {0} |
               if c & 0x0F != 0 {mask & 0x0F} else {0})
            } else {
              mask
            }
      }
      if (!even || !odd || transparency) {
        let oc = mpu.fetchRam(d);
        if !even || (transparency && c & 0x0F == 0) {
          c = (c & 0xF0) | (oc & 0x0F);
        }
        if !odd || (transparency && c & 0xF0 == 0) {
          c = (c & 0x0F) | (oc & 0xF0);
        }
      }
      mpu.store(d, c);
      s = s + sxstride;
      d = d + dxstride;
    }

    if start & 1 != 0 {
      source = (source & 0xff00) | ((source + 1) & 0xff);
    } else {
      source += width as u16;
    }

    if start & 2 != 0 {
      dest = (dest & 0xff00) | ((dest + 1) & 0xff);
    } else {
      dest += width as u16;
    }
  }
}


fn read_roms(roms:&[&str]) -> ~[u8] {
  let a = roms.map(|name| { emu680X::read_file("roms/" + *name) });
  a.iter().fold(~[], |f:~[u8],g| { vec::append(f, *g) })
}

struct Sample {
  time: f64,
  value: u8
}

struct AudioThing {
  samples: ~[Sample]
}

impl AudioThing {

  fn new() -> AudioThing {
    AudioThing { samples: ~[] }
  }

  fn addSample(&mut self, time:f64, value:u8) {
    self.samples.push(Sample{time:time, value:value});
  }

  /*
  fn play(&self) {
    let ctx = 0;// TODO newwebkitAudioContext();
    let RATE = 44100.0;// TODO ctx.sampleRate;
    let b = [];
    let i = 0;
    let v = 0;
    let n = 0;
    while n < self.samples.len() {
      while i / RATE < self.samples[n].time {
        b.push(v);
        i = b.len();
      }
      v = self.samples[n].value;
      n += 1;
    }
    let buf = ctx.createBuffer(1, b.len(), RATE);
    for i in range(0, b.len()) {
      buf.getChannelData(0)[i] = b[i] / 128 - 1;
    }
    let source = ctx.createBufferSource();
    source.buffer = buf;
    source.connect(ctx.destination);
    source.start(0);
  }
  */

}

/*
fn randVideo(mpu:&Cpu) {
  for i in range(0, 304 * 256 / 2) {
    mpu.store(i, i);
  }
}
*/


pub enum RomSet {
  defender,
  defendg,
  defendw,
  stargate,
  robotron,
  robotryo,
  joust,
  joustr,
  joustwr,
  blittest,
}


pub struct Game {
  main: ~Cpu,
  sound: ~Cpu,
  name: ~str,
  label: ~str,
  vertical_count: u8,
  video_counter_address: u16,
  watchdog: u16,
  widgetpia: ~emu680X::Pia6821,
  rompia: ~emu680X::Pia6821,
  soundpia: ~emu680X::Pia6821,
  keymap: ~hashmap::HashMap<char, (*mut emu680X::Pia6821, int, int, ~str)>,
  audio: ~AudioThing,
}

impl Game {

  pub fn new(romset:RomSet) -> Game {

    let mut game = Game {
      main:  ~emu680X::Cpu::new(emu680X::MC6809, 1.000000),
      sound: ~emu680X::Cpu::new(emu680X::MC6808, 0.895000),
      name: ~"",
      label: ~"",
      vertical_count: 0,
      video_counter_address: 0,
      watchdog: 0,
      widgetpia: ~emu680X::Pia6821::new(),
      rompia: ~emu680X::Pia6821::new(),
      soundpia: ~emu680X::Pia6821::new(),
      keymap: ~hashmap::HashMap::new(),
      audio: ~AudioThing::new(),
    };

    // game.main.cyclesPerFrame = 1.0 / 60;       // 1 MHz CPU at 60 Hz refresh rate
    // game.sound.cyclesPerFrame = 0.895 / 60;  // 895 kHz (or so) at 60 Hz

    game.loadRomset(romset);

    game
  }

  fn bank0000(&mut self, _bank:~[u8]) {
    /* TODO
    self.main.hookWrite(0xC900, |b,_| {
      let rom = (b & 1);
      if rom != 0 {
        self.main.loadRom(0x0000, bank.clone());
      } else {
        self.main.unloadRom(0x0000);
      }
      // $("#main .bank").innerHTML = rom ? "ROM" : "DRAM";
    });
    */
  }

  fn bankC000(&mut self, _banks:hashmap::HashMap<int,~[u8]>) {
    /*
    self.main.hookWrite(0xD000, |b,_| {
      // Select ROM bank at C000-CFFF
      // $("#main .bank").innerHTML = b || "I/O";
      self.main.loadRom(0xC000, *banks.get(&(b as int)));
    });
    */
  }

  fn loadRomset(&mut self, romset:RomSet) {
    match romset { blittest => {}, _ => {
      self.main.createRam(0, 0xC010);
      self.sound.createRam(0, 128);
    }}

    // macro_rules! dict(
    //   {$($k:expr : $v:expr),*} => (
    //     {
    //       let mut m = hashmap::HashMap::new();
    //       $(
    //         m.insert($k, $v);
    //       )*
    //       m
    //     }
    //   )
    // )

    match romset {
      defender =>  {
        self.name = ~"Defender";
        self.label = ~"Red";
        self.main.loadRom(0xD000, read_roms(["defend.1", "defend.4",
                                             "defend.2", "defend.3"]));
        // self.bankC000(dict!{
        //   1: read_roms(["defend.9", "defend.12"]),
        //   2: read_roms(["defend.8", "defend.11"]),
        //   3: read_roms(["defend.7", "defend.10"]),
        //   7: read_roms(["defend.6"])
        // });
        self.sound.loadRom(0xF800, read_roms(["defend.snd"]));
      }

      defendg => {
        self.name = ~"Defender";
        self.label = ~"Green";
        self.main.loadRom(0xD000, read_roms(["defeng01.bin", "defeng04.bin",
                                             "defeng02.bin", "defeng03.bin"]));
        // self.bankC000(dict!{
        //   1: read_roms(["defeng09.bin", "defeng12.bin"]),
        //   2: read_roms(["defeng08.bin", "defeng11.bin"]),
        //   3: read_roms(["defeng07.bin", "defeng10.bin"]),
        //   7: read_roms(["defeng06.bin"])
        // });
        self.sound.loadRom(0xF800, read_roms(["defend.snd"]));
      }

      defendw => {
        self.name = ~"Defender";
        self.label = ~"White";
        self.main.loadRom(0xD000, read_roms(["wb01.bin", "defeng02.bin", "wb03.bin"]));
        // self.bankC000(dict!{
        //   1: read_roms(["defeng09.bin", "defeng12.bin"]),
        //   2: read_roms(["defeng08.bin", "defeng11.bin"]),
        //   3: read_roms(["defeng07.bin", "defeng10.bin"]),
        //   7: read_roms(["defeng06.bin"])
        // });
        self.sound.loadRom(0xF800, read_roms(["defend.snd"]));
      }

      stargate => {
        self.name = ~"Stargate";
        self.bank0000(read_roms(["01", "02", "03", "04", "05", "06", "07", "08", "09"]));
        self.main.loadRom(0xD000, read_roms(["10", "11", "12"]));
        self.sound.loadRom(0xF800, read_roms(["sg.snd"]));
      }

      robotron => {
        self.name = ~"Robotron";
        self.label = ~"Solid Blue";
        self.bank0000(read_roms(["robotron.sb1", "robotron.sb2", "robotron.sb3",
                                 "robotron.sb4", "robotron.sb5", "robotron.sb6",
                                 "robotron.sb7", "robotron.sb8", "robotron.sb9"]));
        self.main.loadRom(0xD000, read_roms(["robotron.sba", "robotron.sbb", "robotron.sbc"]));
        self.sound.loadRom(0xF000, read_roms(["robotron.snd"]));
        // Monkey-patch-out the memory test
        // (but not quite right)
        //self.main.patch(0xF472, 0x12);
        //self.main.patch(0xF473, 0x12);
        //self.main.patch(0xF474, 0x12);
      }

      robotryo => {
        self.name = ~"Robotron";
        self.label = ~"Yellow/Orange";
        self.bank0000(read_roms(["robotron.sb1", "robotron.sb2", "robotron.yo3",
                                 "robotron.yo4", "robotron.yo5", "robotron.yo6",
                                 "robotron.yo7", "robotron.sb8", "robotron.sb9"]));
        self.main.loadRom(0xD000, read_roms(["robotron.yoa", "robotron.yob", "robotron.yoc"]));
        self.sound.loadRom(0xF000, read_roms(["robotron.snd"]));
      }

      joust => {
        self.name = ~"Joust";
        self.label = ~"White/Green";
        self.bank0000(read_roms(["3006-13.1b", "3006-14.2b", "3006-15.3b",
                                 "3006-16.4b", "3006-17.5b", "3006-18.6b",
                                 "3006-19.7b", "3006-20.8b", "3006-21.9b"]));
        self.main.loadRom(0xD000, read_roms(["3006-22.10b", "3006-23.11b", "3006-24.12b"]));
        self.sound.loadRom(0xF000, read_roms(["joust.snd"]));
      }

      joustr => {
        self.name = ~"Joust";
        self.label = ~"Solid Red";
        self.bank0000(read_roms(["joust.wg1", "joust.wg2", "joust.wg3",
                                 "joust.sr4", "joust.wg5", "joust.sr6",
                                 "joust.sr7", "joust.sr8", "joust.sr9"]));
        self.main.loadRom(0xD000, read_roms(["joust.sra", "joust.srb", "joust.src"]));
        self.sound.loadRom(0xF000, read_roms(["joust.snd"]));
      }

      joustwr => {
        self.name = ~"Joust";
        self.label = ~"White/Red";
        self.bank0000(read_roms(["joust.wg1", "joust.wg2", "joust.wg3",
                                 "joust.wg4", "joust.wg5", "joust.wg6",
                                 "joust.wr7", "joust.wg8", "joust.wg9"]));
        self.main.loadRom(0xD000, read_roms(["joust.wra", "joust.wgb", "joust.wgc"]));
        self.sound.loadRom(0xF000, read_roms(["joust.snd"]));
      }

      blittest => {
        self.name = ~"Blit Test";
        self.main.loadRam(0x0000, emu680X::read_file("reference/blittest.bin"));
        let pal = [0x00,0xff,0x07,0x05,0x38,0xc7,0xc0,0x80,0xa4,0xe8,0x14,0x90,0x3f,0x51,0x0a,0x67];
        for i in range(0u16, 16u16) {
          self.main.store(0xC000+i, pal[i]);//random());
        }
        // Stub sound
        self.sound.createRam(0, 0x10000);
        for i in range_step(0xF000u16, 0xF030, 3) {
          self.sound.store(i, 0x7E);
          self.sound.storew(i+1, 0xF000);
        }
        self.sound.storew(0xFFFE, 0xF000);
      }
    };

    let rp = ptr::to_mut_unsafe_ptr(self.rompia);
    let wp = ptr::to_mut_unsafe_ptr(self.widgetpia);
    if self.name == ~"Defender" {
      // Older, defender hardware configuration
      self.video_counter_address = 0xC800;
      self.watchdog = 0xC3F3;
      self.main.connectPia(self.widgetpia, 0xCC04);
      self.main.connectPia(self.rompia, 0xCC00);
      self.main.defineIO(0xC010, 0xD001 - 0xC010);
      self.keymap.insert('7', (rp, 0, 0, ~"auto up"));
      self.keymap.insert('8', (rp, 0, 1, ~"advance"));
      self.keymap.insert('6', (rp, 0, 2, ~"right coin"));
      self.keymap.insert('9', (rp, 0, 3, ~"high score reset"));
      self.keymap.insert('5', (rp, 0, 4, ~"left coin"));
      self.keymap.insert('T', (rp, 0, 5, ~"center coin"));
      self.keymap.insert('L', (wp, 0, 0, ~"fire"));
      self.keymap.insert('K', (wp, 0, 1, ~"thrust"));
      self.keymap.insert('B', (wp, 0, 2, ~"spart bomb"));
      self.keymap.insert('H', (wp, 0, 3, ~"hyperspace"));
      self.keymap.insert('2', (wp, 0, 4, ~"2 players"));
      self.keymap.insert('1', (wp, 0, 5, ~"1 player"));
      self.keymap.insert('D', (wp, 0, 6, ~"reverse"));
      self.keymap.insert('S', (wp, 0, 7, ~"down"));
      self.keymap.insert('W', (wp, 1, 0, ~"up"));
    } else {
      // Newer, robotron/joust hardware configuration
      self.video_counter_address = 0xCB00;
      self.watchdog = 0xCBFF;
      self.main.connectPia(self.widgetpia, 0xC804);
      self.main.connectPia(self.rompia, 0xC80C);
      let blt = |startCode,_| {
        let (mask, source, dest, width, height) =
             (self.main.fetch(0xCA01),
              self.main.fetchw(0xCA02),
              self.main.fetchw(0xCA04),
              self.main.fetch(0xCA06),
              self.main.fetch(0xCA07));
        blit(self.main, startCode, mask, source, dest, width, height);
      };
      self.main.hookWrite(0xCA00, blt);
      if self.name != ~"Blit Test" {
        self.main.defineIO(0xC010, 0x1000-0x10);
      }
      // This is for Robotron, but just using it for everything for now.
      self.keymap.insert('E', (wp, 0, 0, ~"move up"));
      self.keymap.insert('D', (wp, 0, 1, ~"move down"));
      self.keymap.insert('S', (wp, 0, 2, ~"move left"));
      self.keymap.insert('F', (wp, 0, 3, ~"move right"));
      self.keymap.insert('1', (wp, 0, 4, ~"1 player start"));
      self.keymap.insert('2', (wp, 0, 5, ~"2 player start"));
      self.keymap.insert('I', (wp, 0, 6, ~"fire up"));
      self.keymap.insert('K', (wp, 0, 7, ~"fire down"));
      self.keymap.insert('J', (wp, 1, 0, ~"fire left"));
      self.keymap.insert('L', (wp, 1, 1, ~"fire right"));
      self.keymap.insert('7', (rp, 0, 0, ~"auto up"));
      self.keymap.insert('8', (rp, 0, 1, ~"advance"));
      self.keymap.insert('6', (rp, 0, 2, ~"right coin"));
      self.keymap.insert('9', (rp, 0, 3, ~"high score reset"));
      self.keymap.insert('5', (rp, 0, 4, ~"left coin"));
      self.keymap.insert('T', (rp, 0, 5, ~"slam door tilt"));
    }

    self.sound.connectPia(self.soundpia, 0x0400);

    let sp = ptr::to_mut_unsafe_ptr(self.soundpia);
    self.rompia.connectPeripheralOutput(1, |value| {
      unsafe { (*sp).peripheralInput(1, value, 0xFF) };
    });

    let _au = ptr::to_mut_unsafe_ptr(self.audio);
    self.soundpia.connectPeripheralOutput(0, |_value| {
      // TODO unsafe { (*au).addSample(self.sound.clock(), value) };
    });
  }


  pub fn run_frame(&mut self) {
    loop {
      if self.sound.clock() < self.main.clock() {
        self.sound.step();
      } else {
        self.main.step();

        let vcount = (self.main.clock() * 60.0 * 256.0) as int as u8;
        if self.vertical_count != vcount {
          self.vertical_count = vcount;
          self.main.store(self.video_counter_address, self.vertical_count & 0xFC);

          if self.vertical_count == 240 || self.vertical_count == 0 {
            self.rompia.peripheralInput(1, 0xFF, 0xFF);
          }
          if self.vertical_count == 0 {
            break;  // That's a vblank, folks
          }
        }
      }
    }
  }

  pub fn handle_key(&self, down:bool, c:char) {
    match self.keymap.find(&c) {
      None => {},
      Some(&(pia, channel, bit, _)) => {
        let mask = 1 << bit;
        unsafe { (*pia).peripheralInput(channel, if down {mask} else {0}, mask) };
      }
    }
  }

  fn system_clock(&self) -> f64 {
    min(self.main.clock(), self.sound.clock())
  }
}


struct Bank {
    roms: hashmap::HashMap<u8, ~[u8]>,
    bank: u8,
}

impl Bank {

    fn new() -> Bank {
        Bank {
            roms: hashmap::HashMap::new(),
            bank: 0xFF,
        }
    }

}



