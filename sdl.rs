// SDL2 bindings

#[allow(dead_code)]

use std::libc::{c_void, c_int, c_uint, c_char, uint8_t, uint16_t, int32_t, uint32_t};
use std::ptr;
use std::cast;
use std::str;
use std::vec;


pub struct Color {
  r: u8,
  g: u8,
  b: u8,
  a: u8
}

impl Clone for Color {
  fn clone(&self) -> Color { Color { r:self.r, g:self.g, b:self.b, a:self.a } }
}

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;

pub struct Window   { p: *SDL_Window   }
pub struct Renderer { p: *SDL_Renderer }
pub struct Texture  { p: *SDL_Texture  }

struct SDL_Rect;

#[link(name="SDL2", kind="framework")]
extern "C" {
  fn SDL_Init(flags: uint32_t) -> c_int;
  fn SDL_CreateWindow(title: *c_char, x: c_int, y: c_int, w: c_int, h: c_int, flags: uint32_t) -> *SDL_Window;
  fn SDL_CreateRenderer(window: *SDL_Window, index: c_int, flags: uint32_t) -> *SDL_Renderer;
  fn SDL_CreateTexture(renderer: *SDL_Renderer, format: uint32_t, access: c_int, w: c_int, h: c_int) -> *SDL_Texture;
  fn SDL_Delay(ms: uint32_t);
  fn SDL_DestroyWindow(window: *SDL_Window);
#[fixed_stack_segment]
  fn SDL_GetError() -> *c_char;
  fn SDL_GetTicks() -> uint32_t;
  fn SDL_PumpEvents();
  fn SDL_PollEvent(event: *SDL_Event) -> c_int;
  fn SDL_RenderCopy(renderer: *SDL_Renderer, texture: *SDL_Texture, srcrect: *SDL_Rect, dstrect: *SDL_Rect) -> c_int;
  fn SDL_RenderPresent(renderer: *SDL_Renderer);
  fn SDL_UpdateTexture(texture: *SDL_Texture, rect: *SDL_Rect, pixels: *c_void, pitch: c_int) -> c_int;
  fn SDL_Quit();
}

pub fn get_error() -> ~str {
    unsafe {
        let cstr = SDL_GetError();
        str::raw::from_c_str(cast::transmute_copy(&cstr))
    }
}

pub fn ticks() -> u32 {
  unsafe { SDL_GetTicks() }
}

static INIT_TIMER          :uint32_t = 0x00000001;
static INIT_AUDIO          :uint32_t = 0x00000010;
static INIT_VIDEO          :uint32_t = 0x00000020;
static INIT_JOYSTICK       :uint32_t = 0x00000200;
static INIT_HAPTIC         :uint32_t = 0x00001000;
static INIT_GAMECONTROLLER :uint32_t = 0x00002000;
static INIT_EVENTS         :uint32_t = 0x00004000;
static INIT_NOPARACHUTE    :uint32_t = 0x00100000;

pub fn init() {
  let result;
  unsafe {
    result = SDL_Init(INIT_VIDEO); // which implies init events
  }
  if result != 0 { fail!("SDL initialization failed"); }
}

static WINDOW_FULLSCREEN: uint32_t = 0x00000001;
static WINDOW_OPENGL: uint32_t = 0x00000002;
static WINDOW_SHOWN: uint32_t = 0x00000004;
static WINDOW_HIDDEN: uint32_t = 0x00000008;
static WINDOW_BORDERLESS: uint32_t = 0x00000010;
static WINDOW_RESIZABLE: uint32_t = 0x00000020;
static WINDOW_MINIMIZED: uint32_t = 0x00000040;
static WINDOW_MAXIMIZED: uint32_t = 0x00000080;
static WINDOW_INPUT_GRABBED: uint32_t = 0x00000100;
static WINDOW_INPUT_FOCUS: uint32_t = 0x00000200;
static WINDOW_MOUSE_FOCUS: uint32_t = 0x00000400;
static WINDOW_FULLSCREEN_DESKTOP: uint32_t = WINDOW_FULLSCREEN | 0x00001000;
static WINDOW_FOREIGN: uint32_t = 0x00000800;
static WINDOW_ALLOW_HIGHDPI: uint32_t = 0x00002000;

static WINDOWPOS_UNDEFINED: c_int = 0x1FFF0000;
static WINDOWPOS_CENTERED:  c_int = 0x2FFF0000;

impl Window {

  pub fn create(title:&str, width:i32, height:i32) -> Window {
    let window = unsafe {
      title.with_c_str(|title| {
        SDL_CreateWindow(title,
                         WINDOWPOS_CENTERED, WINDOWPOS_CENTERED,
                         width as c_int, height as c_int,
                         WINDOW_INPUT_FOCUS)})};
    if window == ptr::null() { fail!("SDL window creation failed"); }
    Window { p: window }
  }

  pub fn create_renderer(&self) -> Renderer {
    let p = unsafe { SDL_CreateRenderer(self.p, -1, 0) };
    if p == ptr::null() { fail!("SDL renderer creation failed"); }
    Renderer { p: p }
  }

  pub fn destroy(&mut self) {
    unsafe {
      SDL_DestroyWindow(self.p);
      self.p = ptr::null();
    }
  }

}


static TEXTUREACCESS_STATIC:    i32 = 0;
static TEXTUREACCESS_STREAMING: i32 = 1;
static TEXTUREACCESS_TARGET:    i32 = 2;

// static PIXELFORMAT_RGB888:   uint32_t = 0x16161804;
// static PIXELFORMAT_BGR888:   uint32_t = 0x16561804;

static PIXELFORMAT_ARGB8888: uint32_t = 0x16362004;
static PIXELFORMAT_RGBA8888: uint32_t = 0x16462004;
static PIXELFORMAT_ABGR8888: uint32_t = 0x16762004;
static PIXELFORMAT_BGRA8888: uint32_t = 0x16862004;

impl Renderer {

  pub fn create_texture(&self, width:int, height:int) -> Texture {
    let p = unsafe { SDL_CreateTexture(self.p, PIXELFORMAT_ABGR8888,
                                       TEXTUREACCESS_STREAMING,
                                       width as c_int, height as c_int) };
    if p == ptr::null() { fail!("SDL texture creation failed"); }
    Texture { p: p }
  }

  pub fn copy_texture(&self, texture:&Texture) {
    let r = unsafe { SDL_RenderCopy(self.p, texture.p, ptr::null(), ptr::null()) };
    if r != 0 { fail!("SDL texture copy failed"); }
  }

  pub fn present(&self) {
    unsafe { SDL_RenderPresent(self.p) };
  }

}

impl Texture {

  pub fn update(&self, pixels:&[Color], pitch:int) {
    let r = unsafe { SDL_UpdateTexture(self.p, ptr::null(), /*vec::raw::to_ptr*/(pixels).as_ptr() as *c_void, pitch as c_int) };
    if r != 0 { fail!("SDL update texture failed {}%s", get_error()); }
  }

}

pub fn poll() -> Event {
  unsafe { SDL_PumpEvents() };
  let event = SDL_Event { data: [0, ..56] };
  let r = unsafe { SDL_PollEvent(&event) };
  if r == 0 {
    NoEvent
  } else {
    event.translate()
  }
}

pub fn delay(ms:u32) {
  unsafe {
    SDL_Delay(ms);
  }
}

pub fn quit() {
  unsafe {
    SDL_Quit();
  }
}

pub enum Event {
  NoEvent,
  UnimplementedEvent,
  QuitEvent,
  KeyEvent(bool, u32, char, bool, bool, bool, bool),
}

struct SDL_Event { data: [uint8_t, ..56u] }

struct SDL_CommonEvent {
  type_: uint32_t,
  timestamp: uint32_t,
}

struct SDL_Keysym {
  scancode: c_uint,
  sym: int32_t,
  mod_: uint16_t,
  unused: uint32_t,
}

struct SDL_KeyboardEvent {
  type_: uint32_t,
  timestamp: uint32_t,
  windowID: uint32_t,
  state: uint8_t,
  repeat: uint8_t,
  padding2: uint8_t,
  padding3: uint8_t,
  keysym: SDL_Keysym,
}

/*
macro_rules! et(
  (${t:ident}) => (
    impl SDL_Event {
      pub fn $t(&self) -> *$t {
        unsafe { cast::transmute_copy(&ptr::to_unsafe_ptr(self)) }
      }
    }
  )
)

et!(SDL_KeyboardEvent);
et!(SDL_CommonEvent);
et!(SDL_QuitEvent);
*/

impl SDL_Event {

  pub fn translate(&self) -> Event {
    let type_ = unsafe { (*self.as_SDL_CommonEvent()).type_ };
    match type_ {

      256 => QuitEvent,

      768 | 769 => {
        let e = unsafe { *self.as_SDL_KeyboardEvent() };
        let ctrl  = e.keysym.mod_ & 0x00C0 != 0;
        let shift = e.keysym.mod_ & 0x0003 != 0;
        let alt   = e.keysym.mod_ & 0x0300 != 0;
        KeyEvent(type_ == 768, e.keysym.scancode, e.keysym.sym as u8 as char,
                 ctrl, shift, alt, e.repeat != 0)
      }

      _ => UnimplementedEvent
    }
  }

  pub fn as_SDL_KeyboardEvent(&self) -> *SDL_KeyboardEvent {
      unsafe { cast::transmute_copy(&ptr::to_unsafe_ptr(self)) }
  }

  pub fn as_SDL_CommonEvent(&self) -> *SDL_CommonEvent {
      unsafe { cast::transmute_copy(&ptr::to_unsafe_ptr(self)) }
  }

}
