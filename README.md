Williams-Rust
=============

Rust port of [my JS version](https://bitbucket.org/grumdrig/williams) of a Williams arcade machine emulator. There's more documentation and notes there.

Shelved until Rust is more mature (or maybe forever, depending on if references and lifetimes get easier to use).