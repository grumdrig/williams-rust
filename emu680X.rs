use std::hashmap;
use std::vec;
use std::rand;
use std::rand::Rng;
use std::io;
use std::path::posix::Path;
use std::ptr;
use std::io::File;


fn read_file(name: ~str) -> ~[u8] {
    File::open(&Path::new(name)).read_to_end()
    //io::read_whole_file(~Path(name)).unwrap()
}


pub enum Dest {
    Address(u16),
    Reg(Register),
    RegPair(Register, Register)
}
impl Dest {
    fn addr(&self) -> u16 {
        match *self {
            Address(a) => a,
            _ => { error!("Expected address"); 0 }
        }
    }
}

enum ResetTarget {
    Reset,
    Vector(u16)
}

struct Disassembly {
    nbytes: u16,
    long: ~str,
    short: ~str
}


fn s8(u: u8) -> int { u as i8 as int }

fn s16(u: u16) -> int { u as i16 as int }

fn x8(n: u8) -> ~str {
    // if (undef(n)) return '__';
    return format!("{:02X}", n as uint);
}

fn x16(n: u16) -> ~str {
    //if (undef(n)) return "____";
    return format!("{:04X}", n as uint);
}

fn hex(n: uint, bits: int) -> ~str {
    if bits == 16 { x16(n as u16) } else { x8(n as u8) }
}

enum AddrMode {
    INHERENT,
    IMMEDIATE,
    LIMMEDIATE,
    DIRECT,
    INDEXED,
    EXTENDED,
    RELATIVE,
    LRELATIVE,
    REGISTER
}

struct ActualFlagSet {
    N: bool,
    Z: bool,
    V: bool,
    C: bool
}

pub enum FlagSet {
    noflags,
    Z,
    NZ,
    NZC,
    NZV,
    NZVC
}

fn actual(set: FlagSet) -> ActualFlagSet {
    match set {
        noflags => ActualFlagSet { N: false, Z: false, V: false, C: false },
        Z       => ActualFlagSet { N: false, Z: true,  V: false, C: false },
        NZ      => ActualFlagSet { N: true,  Z: true,  V: false, C: false },
        NZC     => ActualFlagSet { N: true,  Z: true,  V: false, C: true  },
        NZV     => ActualFlagSet { N: true,  Z: true,  V: true,  C: false },
        NZVC    => ActualFlagSet { N: true,  Z: true,  V: true,  C: true  },
    }
}


type u1 = bool;


pub enum ChipName {
    MC6800,
    MC6802,
    MC6808,
    MC6809,
}

fn name(chip: ChipName) -> &str {
    match chip {
        MC6800 => "MC6800",
        MC6802 => "MC6802",
        MC6808 => "MC6808",
        MC6809 => "MC6809",
    }
}

// type ReadHook<'a>  = &'a fn(u16);
// type WriteHook<'a> = &'a fn (value:u8, address:u16);

enum Legality {
  Legal,
  ILLEGAL,
  FALLBACK
}

enum Register {
    o,  // no register
    A,
    B,
    D,
    X,
    Y,
    S,
    U,
    PC,
    DP,
    CC
}

impl Register {
    fn bits(&self) -> int {
        match *self {
            A  => 8,
            B  => 8,
            D  => 16,
            X  => 16,
            Y  => 16,
            S  => 16,
            U  => 16,
            PC => 16,
            DP => 8,
            CC => 8,
            o  => 0 // fail!("What are you even doing")
        }
    }

    fn name(&self) -> &str {
        match *self {
            o  => "",
            A  => "A",
            B  => "B",
            D  => "D",
            X  => "X",
            Y  => "Y",
            S  => "S",
            U  => "U",
            PC => "PC",
            DP => "DP",
            CC => "CC"
        }
    }
}

fn INSTRUCTIONS_6800(opcode: u8) -> Option<(&str, Register, int, AddrMode)> {
    Some(match opcode {
        0x1B => ("ABA", o, 2, INHERENT),
        0xB9 => ("ADC", A, 4, EXTENDED),
        0xA9 => ("ADC", A, 5, INDEXED),
        0x99 => ("ADC", A, 3, DIRECT),
        0x89 => ("ADC", A, 2, IMMEDIATE),
        0xF9 => ("ADC", B, 4, EXTENDED),
        0xE9 => ("ADC", B, 5, INDEXED),
        0xD9 => ("ADC", B, 3, DIRECT),
        0xC9 => ("ADC", B, 2, IMMEDIATE),
        0xBB => ("ADD", A, 4, EXTENDED),
        0xAB => ("ADD", A, 5, INDEXED),
        0x9B => ("ADD", A, 3, DIRECT),
        0x8B => ("ADD", A, 2, IMMEDIATE),
        0xFB => ("ADD", B, 4, EXTENDED),
        0xEB => ("ADD", B, 5, INDEXED),
        0xDB => ("ADD", B, 3, DIRECT),
        0xCB => ("ADD", B, 2, IMMEDIATE),
        0xB4 => ("AND", A, 4, EXTENDED),
        0xA4 => ("AND", A, 5, INDEXED),
        0x94 => ("AND", A, 3, DIRECT),
        0x84 => ("AND", A, 2, IMMEDIATE),
        0xF4 => ("AND", B, 4, EXTENDED),
        0xE4 => ("AND", B, 5, INDEXED),
        0xD4 => ("AND", B, 3, DIRECT),
        0xC4 => ("AND", B, 2, IMMEDIATE),
        0x78 => ("ASL", o, 6, EXTENDED),
        0x68 => ("ASL", o, 7, INDEXED),
        0x48 => ("ASL", A, 2, INHERENT),
        0x58 => ("ASL", B, 2, INHERENT),
        0x77 => ("ASR", o, 6, EXTENDED),
        0x67 => ("ASR", o, 7, INDEXED),
        0x47 => ("ASR", A, 2, INHERENT),
        0x57 => ("ASR", B, 2, INHERENT),
        0x24 => ("BCC", o, 4, RELATIVE),
        0x25 => ("BCS", o, 4, RELATIVE),
        0x27 => ("BEQ", o, 4, RELATIVE),
        0x2C => ("BGE", o, 4, RELATIVE),
        0x2E => ("BGT", o, 4, RELATIVE),
        0x22 => ("BHI", o, 4, RELATIVE),
        0xB5 => ("BIT", A, 4, EXTENDED),
        0xA5 => ("BIT", A, 5, INDEXED),
        0x95 => ("BIT", A, 3, DIRECT),
        0x85 => ("BIT", A, 2, IMMEDIATE),
        0xF5 => ("BIT", B, 4, EXTENDED),
        0xE5 => ("BIT", B, 5, INDEXED),
        0xD5 => ("BIT", B, 3, DIRECT),
        0xC5 => ("BIT", B, 2, IMMEDIATE),
        0x2F => ("BLE", o, 4, RELATIVE),
        0x23 => ("BLS", o, 4, RELATIVE),
        0x2D => ("BLT", o, 4, RELATIVE),
        0x2B => ("BMI", o, 4, RELATIVE),
        0x26 => ("BNE", o, 4, RELATIVE),
        0x2A => ("BPL", o, 4, RELATIVE),
        0x20 => ("BRA", o, 4, RELATIVE),
        0x8D => ("BSR", o, 8, RELATIVE),
        0x28 => ("BVC", o, 4, RELATIVE),
        0x29 => ("BVS", o, 4, RELATIVE),
        0x11 => ("CBA", o, 2, INHERENT),
        0x0C => ("CLC", o, 2, INHERENT),
        0x0E => ("CLI", o, 2, INHERENT),
        0x7F => ("CLR", o, 6, EXTENDED),
        0x6F => ("CLR", o, 7, INDEXED),
        0x4F => ("CLR", A, 2, INHERENT),
        0x5F => ("CLR", B, 2, INHERENT),
        0x0A => ("CLV", o, 2, INHERENT),
        0xB1 => ("CMP", A, 4, EXTENDED),
        0xA1 => ("CMP", A, 5, INDEXED),
        0x91 => ("CMP", A, 3, DIRECT),
        0x81 => ("CMP", A, 2, IMMEDIATE),
        0xF1 => ("CMP", B, 4, EXTENDED),
        0xE1 => ("CMP", B, 5, INDEXED),
        0xD1 => ("CMP", B, 3, DIRECT),
        0xC1 => ("CMP", B, 2, IMMEDIATE),
        0x73 => ("COM", o, 6, EXTENDED),
        0x63 => ("COM", o, 7, INDEXED),
        0x43 => ("COM", A, 2, INHERENT),
        0x53 => ("COM", B, 2, INHERENT),
        0xBC => ("CP",  X, 5, EXTENDED),
        0xAC => ("CP",  X, 6, INDEXED),
        0x9C => ("CP",  X, 4, DIRECT),
        0x8C => ("CP",  X, 3, LIMMEDIATE),
        0x19 => ("DAA", o, 2, INHERENT),
        0x7A => ("DEC", o, 6, EXTENDED),
        0x6A => ("DEC", o, 7, INDEXED),
        0x4A => ("DEC", A, 2, INHERENT),
        0x5A => ("DEC", B, 2, INHERENT),
        0x34 => ("DE",  S, 4, INHERENT),
        0x09 => ("DE",  X, 4, INHERENT),
        0xB8 => ("EOR", A, 4, EXTENDED),
        0xA8 => ("EOR", A, 5, INDEXED),
        0x98 => ("EOR", A, 3, DIRECT),
        0x88 => ("EOR", A, 2, IMMEDIATE),
        0xF8 => ("EOR", B, 4, EXTENDED),
        0xE8 => ("EOR", B, 5, INDEXED),
        0xD8 => ("EOR", B, 3, DIRECT),
        0xC8 => ("EOR", B, 2, IMMEDIATE),
        0x7C => ("INC", o, 6, EXTENDED),
        0x6C => ("INC", o, 7, INDEXED),
        0x4C => ("INC", A, 2, INHERENT),
        0x5C => ("INC", B, 2, INHERENT),
        0x31 => ("IN",  S, 4, INHERENT),
        0x08 => ("IN",  X, 4, INHERENT),
        0x7E => ("JMP", o, 3, EXTENDED),
        0x6E => ("JMP", o, 4, INDEXED),
        0xBD => ("JSR", o, 8, EXTENDED),
        0xAD => ("JSR", o, 9, INDEXED),
        0xB6 => ("LDA", A, 4, EXTENDED),
        0xA6 => ("LDA", A, 5, INDEXED),
        0x96 => ("LDA", A, 3, DIRECT),
        0x86 => ("LDA", A, 2, IMMEDIATE),
        0xF6 => ("LDA", B, 4, EXTENDED),
        0xE6 => ("LDA", B, 5, INDEXED),
        0xD6 => ("LDA", B, 3, DIRECT),
        0xC6 => ("LDA", B, 2, IMMEDIATE),
        0xBE => ("LD",  S, 5, EXTENDED),
        0xAE => ("LD",  S, 6, INDEXED),
        0x9E => ("LD",  S, 4, DIRECT),
        0x8E => ("LD",  S, 3, LIMMEDIATE),
        0xFE => ("LD",  X, 5, EXTENDED),
        0xEE => ("LD",  X, 6, INDEXED),
        0xDE => ("LD",  X, 4, DIRECT),
        0xCE => ("LD",  X, 3, LIMMEDIATE),
        0x74 => ("LSR", o, 6, EXTENDED),
        0x64 => ("LSR", o, 7, INDEXED),
        0x44 => ("LSR", A, 2, INHERENT),
        0x54 => ("LSR", B, 2, INHERENT),
        0x70 => ("NEG", o, 6, EXTENDED),
        0x60 => ("NEG", o, 7, INDEXED),
        0x40 => ("NEG", A, 2, INHERENT),
        0x50 => ("NEG", B, 2, INHERENT),
        0x01 => ("NOP", o, 2, INHERENT),
        0xBA => ("ORA", A, 4, EXTENDED),
        0xAA => ("ORA", A, 5, INDEXED),
        0x9A => ("ORA", A, 3, DIRECT),
        0x8A => ("ORA", A, 2, IMMEDIATE),
        0xFA => ("ORA", B, 4, EXTENDED),
        0xEA => ("ORA", B, 5, INDEXED),
        0xDA => ("ORA", B, 3, DIRECT),
        0xCA => ("ORA", B, 2, IMMEDIATE),
        0x36 => ("PSH", A, 4, INHERENT),
        0x37 => ("PSH", B, 4, INHERENT),
        0x32 => ("PUL", A, 4, INHERENT),
        0x33 => ("PUL", B, 4, INHERENT),
        0x79 => ("ROL", o,   6, EXTENDED),
        0x69 => ("ROL", o,   7, INDEXED),
        0x49 => ("ROL", A, 2, INHERENT),
        0x59 => ("ROL", B, 2, INHERENT),
        0x76 => ("ROR", o,   6, EXTENDED),
        0x66 => ("ROR", o, 7, INDEXED),
        0x46 => ("ROR", A, 2, INHERENT),
        0x56 => ("ROR", B, 2, INHERENT),
        0x3B => ("RTI", o,10, INHERENT),
        0x39 => ("RTS", o, 5, INHERENT),
        0x10 => ("SBA", o, 2, INHERENT),
        0xB2 => ("SBC", A, 4, EXTENDED),
        0xA2 => ("SBC", A, 5, INDEXED),
        0x92 => ("SBC", A, 3, DIRECT),
        0x82 => ("SBC", A, 2, IMMEDIATE),
        0xF2 => ("SBC", B, 4, EXTENDED),
        0xE2 => ("SBC", B, 5, INDEXED),
        0xD2 => ("SBC", B, 3, DIRECT),
        0xC2 => ("SBC", B, 2, IMMEDIATE),
        0x0D => ("SEC", o, 2, INHERENT),
        0x0F => ("SEI", o, 2, INHERENT),
        0x0B => ("SEV", o, 2, INHERENT),
        0xB7 => ("STA", A, 5, EXTENDED),
        0xA7 => ("STA", A, 6, INDEXED),
        0x97 => ("STA", A, 4, DIRECT),
        0xF7 => ("STA", B, 5, EXTENDED),
        0xE7 => ("STA", B, 6, INDEXED),
        0xD7 => ("STA", B, 4, DIRECT),
        0xBF => ("ST",  S, 6, EXTENDED),
        0xAF => ("ST",  S, 7, INDEXED),
        0x9F => ("ST",  S, 5, DIRECT),
        0xFF => ("ST",  X, 6, EXTENDED),
        0xEF => ("ST",  X, 7, INDEXED),
        0xDF => ("ST",  X, 5, DIRECT),
        0xB0 => ("SUB", A, 4, EXTENDED),
        0xA0 => ("SUB", A, 5, INDEXED),
        0x90 => ("SUB", A, 3, DIRECT),
        0x80 => ("SUB", A, 2, IMMEDIATE),
        0xF0 => ("SUB", B, 4, EXTENDED),
        0xE0 => ("SUB", B, 5, INDEXED),
        0xD0 => ("SUB", B, 3, DIRECT),
        0xC0 => ("SUB", B, 2, IMMEDIATE),
        0x3F => ("SWI", o,12, INHERENT),
        0x16 => ("TAB", o, 2, INHERENT),
        0x06 => ("TAP", o, 2, INHERENT),
        0x17 => ("TBA", o, 2, INHERENT),
        0x07 => ("TPA", o, 2, INHERENT),
        0x7D => ("TST", o, 6, EXTENDED),
        0x6D => ("TST", o, 6, INDEXED),
        0x4D => ("TST", A, 2, INHERENT),
        0x5D => ("TST", B, 2, INHERENT),
        0x30 => ("TSX", o, 4, INHERENT),
        0x35 => ("TXS", o, 4, INHERENT),
        0x3E => ("WAI", o, 9, INHERENT),
        _    => fail!("Invalid opcode")
    })
}

fn INSTRUCTIONS_6809(opcode: u16) -> (&str, Register, AddrMode, int, int, Legality) {
    match opcode {
        0x3A   => ("ABX",  o  , INHERENT ,     3,     1, Legal),
        0x99   => ("ADC",  A  , DIRECT   ,     4,     2, Legal),
        0xB9   => ("ADC",  A  , EXTENDED ,     5,     3, Legal),
        0x89   => ("ADC",  A  , IMMEDIATE,     2,     2, Legal),
        0xA9   => ("ADC",  A  , INDEXED  ,     4,     2, Legal),
        0xD9   => ("ADC",  B  , DIRECT   ,     4,     2, Legal),
        0xF9   => ("ADC",  B  , EXTENDED ,     5,     3, Legal),
        0xC9   => ("ADC",  B  , IMMEDIATE,     2,     2, Legal),
        0xE9   => ("ADC",  B  , INDEXED  ,     4,     2, Legal),
        0x9B   => ("ADD",  A  , DIRECT   ,     4,     2, Legal),
        0xBB   => ("ADD",  A  , EXTENDED ,     5,     3, Legal),
        0x8B   => ("ADD",  A  , IMMEDIATE,     2,     2, Legal),
        0xAB   => ("ADD",  A  , INDEXED  ,     4,     2, Legal),
        0xDB   => ("ADD",  B  , DIRECT   ,     4,     2, Legal),
        0xFB   => ("ADD",  B  , EXTENDED ,     5,     3, Legal),
        0xCB   => ("ADD",  B  , IMMEDIATE,     2,     2, Legal),
        0xEB   => ("ADD",  B  , INDEXED  ,     4,     2, Legal),
        0xD3   => ("ADD",  D  , DIRECT   ,     6,     2, Legal),
        0xF3   => ("ADD",  D  , EXTENDED ,     7,     3, Legal),
        0xC3   => ("ADD",  D  , IMMEDIATE,     4,     3, Legal),
        0xE3   => ("ADD",  D  , INDEXED  ,     6,     2, Legal),
        0x94   => ("AND",  A  , DIRECT   ,     4,     2, Legal),
        0xB4   => ("AND",  A  , EXTENDED ,     5,     3, Legal),
        0x84   => ("AND",  A  , IMMEDIATE,     2,     2, Legal),
        0xA4   => ("AND",  A  , INDEXED  ,     4,     2, Legal),
        0xD4   => ("AND",  B  , DIRECT   ,     4,     2, Legal),
        0xF4   => ("AND",  B  , EXTENDED ,     5,     3, Legal),
        0xC4   => ("AND",  B  , IMMEDIATE,     2,     2, Legal),
        0xE4   => ("AND",  B  , INDEXED  ,     4,     2, Legal),
        0x1C   => ("AND",  CC , IMMEDIATE,     3,     2, Legal),
        0x38   => ("AND",  CC , IMMEDIATE,     4,     2, ILLEGAL),
        0x48   => ("ASL",  A  , INHERENT ,     2,     1, Legal),
        0x58   => ("ASL",  B  , INHERENT ,     2,     1, Legal),
        0x08   => ("ASL",  o  , DIRECT   ,     6,     2, Legal),
        0x78   => ("ASL",  o  , EXTENDED ,     7,     3, Legal),
        0x68   => ("ASL",  o  , INDEXED  ,     6,     2, Legal),
        0x07   => ("ASR",  o  , DIRECT   ,     6,     2, Legal),
        0x77   => ("ASR",  o  , EXTENDED ,     7,     3, Legal),
        0x67   => ("ASR",  o  , INDEXED  ,     6,     2, Legal),
        0x47   => ("ASR",  A  , INHERENT ,     2,     1, Legal),
        0x57   => ("ASR",  B  , INHERENT ,     2,     1, Legal),
        0x27   => ("BEQ",  o  , RELATIVE ,     3,     2, Legal),
        0x2C   => ("BGE",  o  , RELATIVE ,     3,     2, Legal),
        0x2E   => ("BGT",  o  , RELATIVE ,     3,     2, Legal),
        0x22   => ("BHI",  o  , RELATIVE ,     3,     2, Legal),
        0x24   => ("BCC",  o  , RELATIVE ,     3,     2, Legal),
        0x2F   => ("BLE",  o  , RELATIVE ,     3,     2, Legal),
        0x25   => ("BCS",  o  , RELATIVE ,     3,     2, Legal),
        0x23   => ("BLS",  o  , RELATIVE ,     3,     2, Legal),
        0x2D   => ("BLT",  o  , RELATIVE ,     3,     2, Legal),
        0x2B   => ("BMI",  o  , RELATIVE ,     3,     2, Legal),
        0x26   => ("BNE",  o  , RELATIVE ,     3,     2, Legal),
        0x2A   => ("BPL",  o  , RELATIVE ,     3,     2, Legal),
        0x20   => ("BRA",  o  , RELATIVE ,     3,     2, Legal),
        0x21   => ("BRN",  o  , RELATIVE ,     3,     2, Legal),
        0x8D   => ("BSR",  o  , RELATIVE ,     7,     2, Legal),
        0x28   => ("BVC",  o  , RELATIVE ,     3,     2, Legal),
        0x29   => ("BVS",  o  , RELATIVE ,     3,     2, Legal),
        0x95   => ("BIT",  A  , DIRECT   ,     4,     2, Legal),
        0xB5   => ("BIT",  A  , EXTENDED ,     5,     3, Legal),
        0x85   => ("BIT",  A  , IMMEDIATE,     2,     2, Legal),
        0xA5   => ("BIT",  A  , INDEXED  ,     4,     2, Legal),
        0xD5   => ("BIT",  B  , DIRECT   ,     4,     2, Legal),
        0xF5   => ("BIT",  B  , EXTENDED ,     5,     3, Legal),
        0xC5   => ("BIT",  B  , IMMEDIATE,     2,     2, Legal),
        0xE5   => ("BIT",  B  , INDEXED  ,     4,     2, Legal),
        0x0F   => ("CLR",  o  , DIRECT   ,     6,     2, Legal),
        0x7F   => ("CLR",  o  , EXTENDED ,     7,     3, Legal),
        0x6F   => ("CLR",  o  , INDEXED  ,     6,     2, Legal),
        0x4E   => ("CLR",  A  , INHERENT ,     2,     1, ILLEGAL),
        0x4F   => ("CLR",  A  , INHERENT ,     2,     1, Legal),
        0x5E   => ("CLR",  B  , INHERENT ,     2,     1, ILLEGAL),
        0x5F   => ("CLR",  B  , INHERENT ,     2,     1, Legal),
        0x91   => ("CMP",  A  , DIRECT   ,     4,     2, Legal),
        0xB1   => ("CMP",  A  , EXTENDED ,     5,     3, Legal),
        0x81   => ("CMP",  A  , IMMEDIATE,     2,     2, Legal),
        0xA1   => ("CMP",  A  , INDEXED  ,     4,     2, Legal),
        0xD1   => ("CMP",  B  , DIRECT   ,     4,     2, Legal),
        0xF1   => ("CMP",  B  , EXTENDED ,     5,     3, Legal),
        0xC1   => ("CMP",  B  , IMMEDIATE,     2,     2, Legal),
        0xE1   => ("CMP",  B  , INDEXED  ,     4,     2, Legal),
        0x9C   => ("CMP",  X  , DIRECT   ,     6,     2, Legal),
        0xBC   => ("CMP",  X  , EXTENDED ,     7,     3, Legal),
        0x8C   => ("CMP",  X  , IMMEDIATE,     4,     3, Legal),
        0xAC   => ("CMP",  X  , INDEXED  ,     6,     2, Legal),
        0x1093 => ("CMP",  D  , DIRECT   ,     7,     3, Legal),
        0x10B3 => ("CMP",  D  , EXTENDED ,     8,     4, Legal),
        0x1083 => ("CMP",  D  , IMMEDIATE,     5,     4, Legal),
        0x10A3 => ("CMP",  D  , INDEXED  ,     7,     3, Legal),
        0x119C => ("CMP",  S  , DIRECT   ,     7,     3, Legal),
        0x11BC => ("CMP",  S  , EXTENDED ,     8,     4, Legal),
        0x118C => ("CMP",  S  , IMMEDIATE,     5,     4, Legal),
        0x11AC => ("CMP",  S  , INDEXED  ,     7,     3, Legal),
        0x1193 => ("CMP",  U  , DIRECT   ,     7,     3, Legal),
        0x11B3 => ("CMP",  U  , EXTENDED ,     8,     4, Legal),
        0x1183 => ("CMP",  U  , IMMEDIATE,     5,     4, Legal),
        0x11A3 => ("CMP",  U  , INDEXED  ,     7,     3, Legal),
        0x109C => ("CMP",  Y  , DIRECT   ,     7,     3, Legal),
        0x10BC => ("CMP",  Y  , EXTENDED ,     8,     4, Legal),
        0x108C => ("CMP",  Y  , IMMEDIATE,     5,     4, Legal),
        0x10AC => ("CMP",  Y  , INDEXED  ,     7,     3, Legal),
        0x03   => ("COM",  o  , DIRECT   ,     6,     2, Legal),
        0x73   => ("COM",  o  , EXTENDED ,     7,     3, Legal),
        0x63   => ("COM",  o  , INDEXED  ,     6,     2, Legal),
        0x43   => ("COM",  A  , INHERENT ,     2,     1, Legal),
        0x53   => ("COM",  B  , INHERENT ,     2,     1, Legal),
        0x3C   => ("CWAI", o  , INHERENT ,    21,     2, Legal),
        0x19   => ("DAA",  o  , INHERENT ,     2,     1, Legal),
        0x0A   => ("DEC",  o  , DIRECT   ,     6,     2, Legal),
        0x0B   => ("DEC",  o  , DIRECT   ,     6,     2, ILLEGAL),
        0x7A   => ("DEC",  o  , EXTENDED ,     7,     3, Legal),
        0x7B   => ("DEC",  o  , EXTENDED ,     7,     3, ILLEGAL),
        0x6A   => ("DEC",  o  , INDEXED  ,     6,     2, Legal),
        0x6B   => ("DEC",  o  , INDEXED  ,     6,     2, ILLEGAL),
        0x4A   => ("DEC",  A  , INHERENT ,     2,     1, Legal),
        0x4B   => ("DEC",  A  , INHERENT ,     2,     1, ILLEGAL),
        0x5A   => ("DEC",  B  , INHERENT ,     2,     1, Legal),
        0x5B   => ("DEC",  B  , INHERENT ,     2,     1, ILLEGAL),
        0x98   => ("EOR",  A  , DIRECT   ,     4,     2, Legal),
        0xB8   => ("EOR",  A  , EXTENDED ,     5,     3, Legal),
        0x88   => ("EOR",  A  , IMMEDIATE,     2,     2, Legal),
        0xA8   => ("EOR",  A  , INDEXED  ,     4,     2, Legal),
        0xD8   => ("EOR",  B  , DIRECT   ,     4,     2, Legal),
        0xF8   => ("EOR",  B  , EXTENDED ,     5,     3, Legal),
        0xC8   => ("EOR",  B  , IMMEDIATE,     2,     2, Legal),
        0xE8   => ("EOR",  B  , INDEXED  ,     4,     2, Legal),
        0x1E   => ("EXG",  o  , REGISTER ,     8,     2, Legal),
        0x14   => ("!HCF", o  , INHERENT ,     2,     1, ILLEGAL),
        0x15   => ("!HCF", o  , INHERENT ,     2,     1, ILLEGAL),
        0xCD   => ("!HCF", o  , INHERENT ,     2,     1, ILLEGAL),
        0x0C   => ("INC",  o  , DIRECT   ,     6,     2, Legal),
        0x7C   => ("INC",  o  , EXTENDED ,     7,     3, Legal),
        0x6C   => ("INC",  o  , INDEXED  ,     6,     2, Legal),
        0x4C   => ("INC",  A  , INHERENT ,     2,     1, Legal),
        0x5C   => ("INC",  B  , INHERENT ,     2,     1, Legal),
        0x0E   => ("JMP",  o  , DIRECT   ,     3,     2, Legal),
        0x7E   => ("JMP",  o  , EXTENDED ,     3,     3, Legal),
        0x6E   => ("JMP",  o  , INDEXED  ,     3,     2, Legal),
        0x9D   => ("JSR",  o  , DIRECT   ,     7,     2, Legal),
        0xBD   => ("JSR",  o  , EXTENDED ,     8,     3, Legal),
        0xAD   => ("JSR",  o  , INDEXED  ,     7,     2, Legal),
        0x02   => ("LBRA", o  , LRELATIVE,     5,     3, ILLEGAL),
        0x16   => ("LBRA", o  , LRELATIVE,     5,     3, Legal),
        0x17   => ("LBSR", o  , LRELATIVE,     9,     3, Legal),
        0x1027 => ("LBEQ", o  , LRELATIVE,     5,     4, Legal),
        0x102C => ("LBGE", o  , LRELATIVE,     5,     4, Legal),
        0x102E => ("LBGT", o  , LRELATIVE,     5,     4, Legal),
        0x1022 => ("LBHI", o  , LRELATIVE,     5,     4, Legal),
        0x1024 => ("LBCC", o  , LRELATIVE,     5,     4, Legal),
        0x102F => ("LBLE", o  , LRELATIVE,     5,     4, Legal),
        0x1025 => ("LBCS", o  , LRELATIVE,     5,     4, Legal),
        0x1023 => ("LBLS", o  , LRELATIVE,     5,     4, Legal),
        0x102D => ("LBLT", o  , LRELATIVE,     5,     4, Legal),
        0x102B => ("LBMI", o  , LRELATIVE,     5,     4, Legal),
        0x1026 => ("LBNE", o  , LRELATIVE,     5,     4, Legal),
        0x102A => ("LBPL", o  , LRELATIVE,     5,     4, Legal),
        0x1021 => ("LBRN", o  , LRELATIVE,     5,     4, Legal),
        0x1028 => ("LBVC", o  , LRELATIVE,     5,     4, Legal),
        0x1029 => ("LBVS", o  , LRELATIVE,     5,     4, Legal),
        0x96   => ("LD",   A  , DIRECT   ,     4,     2, Legal),
        0xB6   => ("LD",   A  , EXTENDED ,     5,     3, Legal),
        0x86   => ("LD",   A  , IMMEDIATE,     2,     2, Legal),
        0xA6   => ("LD",   A  , INDEXED  ,     4,     2, Legal),
        0xD6   => ("LD",   B  , DIRECT   ,     4,     2, Legal),
        0xF6   => ("LD",   B  , EXTENDED ,     5,     3, Legal),
        0xC6   => ("LD",   B  , IMMEDIATE,     2,     2, Legal),
        0xE6   => ("LD",   B  , INDEXED  ,     4,     2, Legal),
        0xDC   => ("LD",   D  , DIRECT   ,     5,     2, Legal),
        0xFC   => ("LD",   D  , EXTENDED ,     6,     3, Legal),
        0xCC   => ("LD",   D  , IMMEDIATE,     3,     3, Legal),
        0xEC   => ("LD",   D  , INDEXED  ,     5,     2, Legal),
        0xDE   => ("LD",   U  , DIRECT   ,     5,     2, Legal),
        0xFE   => ("LD",   U  , EXTENDED ,     6,     3, Legal),
        0xCE   => ("LD",   U  , IMMEDIATE,     3,     3, Legal),
        0xEE   => ("LD",   U  , INDEXED  ,     5,     2, Legal),
        0x9E   => ("LD",   X  , DIRECT   ,     5,     2, Legal),
        0xBE   => ("LD",   X  , EXTENDED ,     6,     3, Legal),
        0x8E   => ("LD",   X  , IMMEDIATE,     3,     3, Legal),
        0xAE   => ("LD",   X  , INDEXED  ,     5,     2, Legal),
        0x108E => ("LD",   Y  , IMMEDIATE,     4,     4, Legal),
        0x10DE => ("LD",   S  , DIRECT   ,     6,     3, Legal),
        0x10FE => ("LD",   S  , EXTENDED ,     7,     4, Legal),
        0x10CE => ("LD",   S  , IMMEDIATE,     4,     4, Legal),
        0x10EE => ("LD",   S  , INDEXED  ,     6,     3, Legal),
        0x109E => ("LD",   Y  , DIRECT   ,     6,     3, Legal),
        0x10BE => ("LD",   Y  , EXTENDED ,     7,     4, Legal),
        0x10AE => ("LD",   Y  , INDEXED  ,     6,     3, Legal),
        0x32   => ("LEA",  S  , INDEXED  ,     4,     2, Legal),
        0x33   => ("LEA",  U  , INDEXED  ,     4,     2, Legal),
        0x30   => ("LEA",  X  , INDEXED  ,     4,     2, Legal),
        0x31   => ("LEA",  Y  , INDEXED  ,     4,     2, Legal),
        0x04   => ("LSR",  o  , DIRECT   ,     6,     2, Legal),
        0x05   => ("LSR",  o  , DIRECT   ,     6,     2, ILLEGAL),
        0x74   => ("LSR",  o  , EXTENDED ,     7,     3, Legal),
        0x75   => ("LSR",  o  , EXTENDED ,     7,     3, ILLEGAL),
        0x64   => ("LSR",  o  , INDEXED  ,     6,     2, Legal),
        0x65   => ("LSR",  o  , INDEXED  ,     6,     2, ILLEGAL),
        0x44   => ("LSR",  A  , INHERENT ,     2,     1, Legal),
        0x45   => ("LSR",  A  , INHERENT ,     2,     1, ILLEGAL),
        0x54   => ("LSR",  B  , INHERENT ,     2,     1, Legal),
        0x55   => ("LSR",  B  , INHERENT ,     2,     1, ILLEGAL),
        0x3D   => ("MUL",  o  , INHERENT ,    11,     1, Legal),
        0x00   => ("NEG",  o  , DIRECT   ,     6,     2, Legal),
        0x01   => ("NEG",  o  , DIRECT   ,     6,     2, ILLEGAL),
        0x70   => ("NEG",  o  , EXTENDED ,     7,     3, Legal),
        0x71   => ("NEG",  o  , EXTENDED ,     7,     3, ILLEGAL),
        0x72   => ("NECO", o  , EXTENDED ,     7,     3, ILLEGAL),
        0x60   => ("NEG",  o  , INDEXED  ,     6,     2, Legal),
        0x61   => ("NEG",  o  , INDEXED  ,     6,     2, ILLEGAL),
        0x62   => ("NECO", o  , INDEXED  ,     6,     2, ILLEGAL),
        0x40   => ("NEG",  A  , INHERENT ,     2,     1, Legal),
        0x41   => ("NEG",  A  , INHERENT ,     2,     1, ILLEGAL),
        0x42   => ("NECO", A  , INHERENT ,     2,     1, ILLEGAL),
        0x50   => ("NEG",  B  , INHERENT ,     2,     1, Legal),
        0x51   => ("NEG",  B  , INHERENT ,     2,     1, ILLEGAL),
        0x52   => ("NECO", B  , INHERENT ,     2,     1, ILLEGAL),
        0x12   => ("NOP",  o  , INHERENT ,     2,     1, Legal),
        0x1B   => ("NOP",  o  , INHERENT ,     2,     1, ILLEGAL),
        0x87   => ("DROP", o  , IMMEDIATE,     2,     2, ILLEGAL),
        0xC7   => ("DROP", o  , IMMEDIATE,     2,     2, ILLEGAL),
        0x9A   => ("OR",   A  , DIRECT   ,     4,     2, Legal),
        0xBA   => ("OR",   A  , EXTENDED ,     5,     3, Legal),
        0x8A   => ("OR",   A  , IMMEDIATE,     2,     2, Legal),
        0xAA   => ("OR",   A  , INDEXED  ,     4,     2, Legal),
        0xDA   => ("OR",   B  , DIRECT   ,     4,     2, Legal),
        0xFA   => ("OR",   B  , EXTENDED ,     5,     3, Legal),
        0xCA   => ("OR",   B  , IMMEDIATE,     2,     2, Legal),
        0xEA   => ("OR",   B  , INDEXED  ,     4,     2, Legal),
        0x1A   => ("OR",   CC , IMMEDIATE,     3,     2, Legal),
        0x34   => ("PSHS", o  , IMMEDIATE,     5,     2, Legal),
        0x36   => ("PSHU", o  , IMMEDIATE,     5,     2, Legal),
        0x35   => ("PULS", o  , IMMEDIATE,     5,     2, Legal),
        0x37   => ("PULU", o  , IMMEDIATE,     5,     2, Legal),
        0x3E   =>("RESET", o  , INHERENT ,    15,     1, ILLEGAL),
        0x09   => ("ROL",  o  , DIRECT   ,     6,     2, Legal),
        0x79   => ("ROL",  o  , EXTENDED ,     7,     3, Legal),
        0x69   => ("ROL",  o  , INDEXED  ,     6,     2, Legal),
        0x49   => ("ROL",  A  , INHERENT ,     2,     1, Legal),
        0x59   => ("ROL",  B  , INHERENT ,     2,     1, Legal),
        0x06   => ("ROR",  o  , DIRECT   ,     6,     2, Legal),
        0x76   => ("ROR",  o  , EXTENDED ,     7,     3, Legal),
        0x66   => ("ROR",  o  , INDEXED  ,     6,     2, Legal),
        0x46   => ("ROR",  A  , INHERENT ,     2,     1, Legal),
        0x56   => ("ROR",  B  , INHERENT ,     2,     1, Legal),
        0x3B   => ("RTI",  o  , INHERENT ,     6,     1, Legal),
        0x39   => ("RTS",  o  , INHERENT ,     5,     1, Legal),
        0x92   => ("SBC",  A  , DIRECT   ,     4,     2, Legal),
        0xB2   => ("SBC",  A  , EXTENDED ,     5,     3, Legal),
        0x82   => ("SBC",  A  , IMMEDIATE,     2,     2, Legal),
        0xA2   => ("SBC",  A  , INDEXED  ,     4,     2, Legal),
        0xD2   => ("SBC",  B  , DIRECT   ,     4,     2, Legal),
        0xF2   => ("SBC",  B  , EXTENDED ,     5,     3, Legal),
        0xC2   => ("SBC",  B  , IMMEDIATE,     2,     2, Legal),
        0xE2   => ("SBC",  B  , INDEXED  ,     4,     2, Legal),
        0x1D   => ("SEX",  o  , INHERENT ,     2,     1, Legal),
        0x97   => ("ST",   A  , DIRECT   ,     4,     2, Legal),
        0xB7   => ("ST",   A  , EXTENDED ,     5,     3, Legal),
        0xA7   => ("ST",   A  , INDEXED  ,     4,     2, Legal),
        0xD7   => ("ST",   B  , DIRECT   ,     4,     2, Legal),
        0xF7   => ("ST",   B  , EXTENDED ,     5,     3, Legal),
        0xE7   => ("ST",   B  , INDEXED  ,     4,     2, Legal),
        0xDD   => ("ST",   D  , DIRECT   ,     5,     2, Legal),
        0xFD   => ("ST",   D  , EXTENDED ,     6,     3, Legal),
        0xED   => ("ST",   D  , INDEXED  ,     5,     2, Legal),
        0xDF   => ("ST",   U  , DIRECT   ,     5,     2, Legal),
        0xFF   => ("ST",   U  , EXTENDED ,     6,     3, Legal),
        0xEF   => ("ST",   U  , INDEXED  ,     5,     2, Legal),
        0x9F   => ("ST",   X  , DIRECT   ,     5,     2, Legal),
        0xBF   => ("ST",   X  , EXTENDED ,     6,     3, Legal),
        0xAF   => ("ST",   X  , INDEXED  ,     5,     2, Legal),
        0x10EF => ("ST",   S  , INDEXED  ,     6,     3, Legal),
        0x10DF => ("ST",   S  , DIRECT   ,     6,     3, Legal),
        0x10FF => ("ST",   S  , EXTENDED ,     7,     4, Legal),
        0x109F => ("ST",   Y  , DIRECT   ,     6,     3, Legal),
        0x10BF => ("ST",   Y  , EXTENDED ,     7,     4, Legal),
        0x10AF => ("ST",   Y  , INDEXED  ,     6,     3, Legal),
        0x90   => ("SUB",  A  , DIRECT   ,     4,     2, Legal),
        0xB0   => ("SUB",  A  , EXTENDED ,     5,     3, Legal),
        0x80   => ("SUB",  A  , IMMEDIATE,     2,     2, Legal),
        0xA0   => ("SUB",  A  , INDEXED  ,     4,     2, Legal),
        0xD0   => ("SUB",  B  , DIRECT   ,     4,     2, Legal),
        0xF0   => ("SUB",  B  , EXTENDED ,     5,     3, Legal),
        0xC0   => ("SUB",  B  , IMMEDIATE,     2,     2, Legal),
        0xE0   => ("SUB",  B  , INDEXED  ,     4,     2, Legal),
        0x93   => ("SUB",  D  , DIRECT   ,     6,     2, Legal),
        0xB3   => ("SUB",  D  , EXTENDED ,     7,     3, Legal),
        0x83   => ("SUB",  D  , IMMEDIATE,     4,     3, Legal),
        0xA3   => ("SUB",  D  , INDEXED  ,     6,     2, Legal),
        0x3F   => ("SWI",  o  , INHERENT ,    19,     1, Legal),
        0x103F => ("SWI2", o  , INHERENT ,    20,     2, Legal),
        0x113F => ("SWI3", o  , INHERENT ,    20,     2, Legal),
        0x13   => ("SYNC", o  , INHERENT ,     4,     1, Legal),
        0x1F   => ("TFR",  o  , REGISTER ,     7,     2, Legal),
        0x0D   => ("TST",  o  , DIRECT   ,     6,     2, Legal),
        0x7D   => ("TST",  o  , EXTENDED ,     7,     3, Legal),
        0x6D   => ("TST",  o  , INDEXED  ,     6,     2, Legal),
        0x4D   => ("TST",  A  , INHERENT ,     2,     1, Legal),
        0x5D   => ("TST",  B  , INHERENT ,     2,     1, Legal),
        0x1010=>("!TRACE", o  , INHERENT ,     2,     2, ILLEGAL),
        0x1011 => ("!WTF", o  , INHERENT ,     2,     2, ILLEGAL),
        0x1110 => ("!WTF", o  , INHERENT ,     2,     2, ILLEGAL),
        0x1111 => ("!WTF", o  , INHERENT ,     2,     2, ILLEGAL),
        0x18   => ("!SCC", o  , INHERENT ,     3,     1, ILLEGAL),
        _ if opcode >> 8 == 0x10 || opcode >> 8 == 0x11 => {
            let (mn, r, m, c, b, _) = INSTRUCTIONS_6809(opcode & 0xFF);
            (mn, r, m, c, b, FALLBACK)
        },
        _      => fail!("Invalid opcode")
    }
}


// #[deriving(Default)]
struct Cpu {
    E: u1,
    F: u1,
    H: u1,
    I: u1,
    N: u1,
    Z: u1,
    V: u1,
    C: u1,

    A: u8,
    B: u8,
    DP: u8,
    X: u16,
    Y: u16,
    S: u16,
    U: u16,
    PC: u16,

    s_initialized: bool,

    CLOCK: int,
    MCLOCK: int,

    verbose: bool,

    partNumber: ChipName,
    clockRate: f64,
    stackAdjust: u16,

    RESET: bool,
    NMI: bool,
    IRQ: bool,
    FIRQ: bool,
    WAIT: bool,

    SYNC: bool,

    ROM: hashmap::HashMap<u16, ~[u8]>,
    RAM: ~[u8],
    IO: hashmap::HashMap<u16, u8>,
    IOstart: u16,
    IOlength: u16,

    PIAS: ~[*mut Pia6821],

    // WRITE_HOOKS: ~hashmap::HashMap<u16, &'a fn(u8,u16)>,
    // READ_HOOKS:  ~hashmap::HashMap<u16, &'a fn(u16)>,
}


fn bit(b: bool) -> u8 { if b {1} else {0} }
fn bit16(b: bool) -> u16 { if b {1} else {0} }

// S is considered "uninitialized" when it has this value. NMIs are disabled
// until such time as S is initialized. This is cheesy, but, will only fail if
// S happens to have this value when a NMI is triggered for the first time.

static UNINITIALIZED:u16 = 0xEEEE;


impl Cpu {
    pub fn new(part: ChipName, rate: f64) -> Cpu {
        // let cpu = Cpu {}; //Default::default();
        Cpu {
            // ..Default::default()
            E: false,
            F: false,
            H: false,
            I: false,
            N: false,
            Z: false,
            V: false,
            C: false,

            A: 0,
            B: 0,
            DP: 0,
            X: 0,
            Y: 0,
            S: UNINITIALIZED,
            U: 0,
            PC: 0,

            s_initialized: false,

            CLOCK: 0,
            MCLOCK: 0,

            verbose: false,

            partNumber: part,
            clockRate: rate,
            stackAdjust: match part { MC6809 => 0, _ => 1 }, // 6800 stack points below last push, 6809 points at it

            RESET: true,
            NMI: false,
            IRQ: false,
            FIRQ: false,
            WAIT: false,

            SYNC: false,

            ROM: hashmap::HashMap::new(),
            RAM: ~[],
            IO: hashmap::HashMap::new(),
            IOstart: 0,
            IOlength: 0,
            PIAS: ~[],
        }
    }

    fn setIRQ(&mut self, v:bool) { self.IRQ = v; }

    fn CC(&self) -> u8 {
        bit(self.E) << 7 | bit(self.F) << 6 | bit(self.H) << 5 | bit(self.I) << 4 |
        bit(self.N) << 3 | bit(self.Z) << 2 | bit(self.V) << 1 | bit(self.C)
    }

    fn set_CC(&mut self, v: u8) {
        self.E = 1 == 1 & v >> 7;
        self.F = 1 == 1 & v >> 6;
        self.H = 1 == 1 & v >> 5;
        self.I = 1 == 1 & v >> 4;
        self.N = 1 == 1 & v >> 3;
        self.Z = 1 == 1 & v >> 2;
        self.V = 1 == 1 & v >> 1;
        self.C = 1 == 1 & v;
    }

    fn D(&self) -> u16 {
        (self.A as u16 << 8) + (self.B as u16)
    }

    fn set_D(&mut self, v: u16) {
        self.A = (v >> 8) as u8;
        self.B = v as u8;
    }

    fn traceBytes(&self, start: u16, bytes: u16, mut width: int, sep: &str) -> ~str {
        if width == 0 { width = bytes as int };
        let mut bs = ~"";
        for i in range(0, width) {
            bs = bs + (if i < bytes as int {
                x8(self.fetch8(start + i as u16, true, false))
            } else {
                ~"  "
            }) + sep.clone();
        }
        return bs;
    }

    fn disassemble6800(&self, pc: u16) -> ~Disassembly {
        let opcode = self.fetch8(pc, true, false);
        let mut nbytes = 1;
        let (mn, register, _, mode) = INSTRUCTIONS_6800(opcode).unwrap();
        let mnemonic = mn + register.name();

        let mut operand;

        match (mode) {
            INHERENT => {
                operand = ~"";
            }
            RELATIVE => {
                nbytes = 2;
                let addr = (pc as int + 2 + s8(self.fetch8(pc + 1, true, false))) as u16;
                operand = ~"L_" + x16(addr);
            }
            EXTENDED => {
                nbytes = 3;
                let addr = self.fetch16(pc + 1, true);
                operand = ~"$" + x16(addr)
            }
            DIRECT => {
                nbytes = 2;
                let addr = self.fetch8(pc + 1, true, false) as u16;
                operand = ~"<$" + x16(addr)
            }
            INDEXED => {
                nbytes = 2;
                let a = self.fetch8(pc + 1, true, false);
                operand = ~"X + $" + x8(a);
            }
            LIMMEDIATE => {
                let addr = pc + 1;
                nbytes = 3;
                operand = ~"#$" + x16(self.fetch16(addr, true));
            }
            IMMEDIATE => {
                let addr = pc + 1;
                nbytes = 2;
                operand = ~"#$" + x8(self.fetch8(addr, true, false));
            }
            LRELATIVE | REGISTER => fail!("Impossible addressing mode for 6800")
        }

        let bytes = self.traceBytes(pc, nbytes, 3, " ");
        let stack = self.traceBytes(self.S+1, 4, 4, ",");
        let state = (x8(self.A) + ":" + x8(self.B) + " <" +
                     x16(self.X) + " " + x16(self.S) + "[" + stack + "] " +
                     (if self.H {"h"} else {"."}) +
                     (if self.I {"i"} else {"."}) +
                     (if self.N {"n"} else {"."}) +
                     (if self.Z {"z"} else {"."}) +
                     (if self.V {"v"} else {"."}) +
                     (if self.C {"c"} else {"."}) + " ");
        ~Disassembly {
            nbytes: nbytes,
            long:  format!("{:04X}:  {:s} {:s}    {:s} {:s}", pc as uint, state, bytes, mnemonic, operand),
            short: format!("{:04X}:  {:s}    {:s} {:s}",    pc as uint,        bytes, mnemonic, operand)
        }
    }

    fn disassemble6809(&self, pc: u16) -> ~Disassembly {
        let mut opcode = self.fetch8(pc, true, false) as u16;
        let mut ppc = pc + 1;
        if opcode == 0x10 || opcode == 0x11 {
            opcode = opcode * 256 + self.fetch8(ppc, true, false) as u16;
            ppc += 1;
        }

        let (mn, register, mode, _, nb, _) = INSTRUCTIONS_6809(opcode);
        let mut nbytes = nb;
        let mnemonic = mn + register.name();

        let width = register.bits();

        let mut param: ~str;
        match mode {
            EXTENDED => {
                param = format!("${:04X}", self.fetch16(ppc, true) as uint);

            }
            INHERENT => {
                param = ~"";

            }
            RELATIVE => {
                param = format!("L_{:04X}", (ppc as int + 1 + s8(self.fetch8(ppc, true, false))) as uint);

            }
            LRELATIVE => {
                param = format!("L_{:04X}", (ppc as int + 2 + s16(self.fetch16(ppc, true))) as uint);

            }
            DIRECT => {
                let offset = self.fetch8(ppc, true, false);
                let addr = self.DP * 256 + offset;
                param = format!("DP:${:02X} ;= ${:04X}", offset as uint, addr as uint);

            }
            REGISTER => {
                let TFR_REGS = ["D", "X", "Y", "U", "S", "PC", "??", "??",
                                "A", "B", "CC", "DP"];
                let postbyte = self.fetch8(ppc, true, false);
                param = format!("{:s}, {:s}", TFR_REGS[postbyte & 0xF], TFR_REGS[postbyte >> 4]);

            }
            IMMEDIATE => {
                param = format!("\\#${:s}", if (width == 16)
                            { x16(self.fetch16(ppc, true)) } else
                            { x8(self.fetch8(ppc, true, false)) });

            }
            INDEXED => {
                // For this mode we"ll update ppc in order to correct the value of nbytes
                let postbyte = self.fetch8(ppc, true, false);
                ppc += 1;
                let n = (postbyte >> 5) as uint & 0x3;
                let subregister = [X,Y,U,S][n];
                let indirect = ((postbyte & 0x10) != 0);
                let mut submode = postbyte & 0xF;
                if postbyte == 0x9F {
                    // Extended indirect
                    let ptr = self.fetch16(ppc, true);
                    ppc += 2;
                    let a = self.fetch16(ptr, true);
                    param = format!("[{:04X}] ;= {:04X}", ptr as uint, a as uint);
                } else if (postbyte & 0x80) == 0 {
                    // 5-bit offset from register contained in postbyte
                    if indirect { submode -= 0x10; }    // account for sign bit
                    let a = self.reg(subregister) + submode as u16;
                    param = format!("{:s}, {:u} ;= ${:04X}", subregister.name(), submode as uint, a as uint);
                } else {
                    let mut addr = self.reg(subregister);
                    param = ~"" + subregister.name();
                    let offset = if (submode == 0x4) {
                        0
                    } else if (submode == 0x8) {
                        ppc += 1;
                        s8(self.fetch8(ppc - 1, true, false))
                    } else if (submode == 0x9) {
                        ppc += 2;
                        s16(self.fetch16(ppc, true))
                    } else if (submode == 0x6) {
                        param = subregister.name() + ", A";
                        self.A as int
                    } else if (submode == 0x5) {
                        param = subregister.name() + ", B";
                        self.B as int
                    } else if (submode == 0xB) {
                        param = subregister.name() + ", D";
                        self.D() as int
                    } else if (submode == 0) {
                        param = param + "+";
                        0
                    } else if (submode == 1) {
                        param = param + "++";
                        0
                    } else if (submode == 2) {
                        param = "-" + param;
                        -1    // it"s a pre-decrement
                    } else if (submode == 3) {
                        param = "--" + param;
                        -2    // it"s a pre-decrement
                    } else if (submode == 0xC) {
                        addr = 0;
                        let offset = s8(self.fetch8(ppc, true, false));
                        ppc += 1;
                        param = offset.to_str();
                        offset
                    } else if (submode == 0xD) {
                        addr = 0;
                        let offset = s16(self.fetch16(ppc, true));
                        ppc += 2;
                        param = offset.to_str();
                        offset
                    } else {
                        param = ~"??";
                        0
                    };

                    if subregister.name() == param && offset != 0 {
                        param = param + ", " + offset.to_str();
                    }

                    addr = (addr as int + offset) as u16;
                    param = format!("{:s} ;= {:04X}", param, addr as uint);

                    if (indirect) {
                        addr = self.fetch16(addr, true);
                        param = format!("[{:s}] ;= {:04X}", param, addr as uint);
                    }
                }
                nbytes = ppc as int - pc as int;
            }
            LIMMEDIATE => fail!("Impossible addressing mode")
        }

        fn flg(v: u1, f: ~str) -> ~str { if v {f} else {~"-"} }
        let state = format!("<{:02X} {:02X}:{:02X} x{:4X} y{:4X} s{:4X}[{:s}] u{:4X}[]",
                         self.DP as uint, self.A as uint, self.B as uint,
                         self.X as uint, self.Y as uint, self.S as uint,
                         self.traceBytes(self.S,4,4,","), self.U as uint) +
                         flg(self.E, ~"e") +
                         flg(self.F, ~"f") +
                         flg(self.H, ~"h") +
                         flg(self.I, ~"i") +
                         flg(self.N, ~"n") +
                         flg(self.Z, ~"z") +
                         flg(self.V, ~"v") +
                         flg(self.C, ~"c");

        let bytes = self.traceBytes(pc, nbytes as u16, 5, " ");

        ~Disassembly {
            nbytes: nbytes as u16,
            long:  format!("{:04X}: {} {}    {} {}", pc, state, bytes, mnemonic, param),
            short: format!("{:04X}:    {}    {} {}", pc,        bytes, mnemonic, param),
        }
    }

    pub fn unloadRom(&mut self, addr: u16) {
        self.ROM.remove(&addr);
    }

    pub fn loadRom(&mut self, addr: u16, bytes:~[u8]) {
        self.ROM.insert(addr, bytes);
    }

    pub fn defineIO(&mut self, start: u16, length: u16) {
        self.IOstart = start;
        self.IOlength = length;
    }

/* TODO get to this later
    fn loadSrec(&self, file:~[u8]) {
        // http://en.wikipedia.org/wiki/SREC_(file_format)
        for i in range(0, file.len()) {
            let line = file[i];
            let command = line.substr(0,2);
            if (command == "S9") { break; }
            if (command != "S1") {
                self.error("Unrecognized SREC record type: " + command);
            }
            let bytecount = parseInt(line.substr(2,2), 16);
            let address = parseInt(line.substr(4,4), 16);
            let bytes = [];
            for j in range(0, bytecount - 3) {
                let v = parseInt(line.substr(8 + j * 2, 2), 16);
                self.store8(address, v);
                address += 1;
            }
        }
    }
*/

    pub fn loadRam(&mut self, addr: u16, data: ~[u8]) {
        assert!(addr == 0); // Unimplected: RAM elsewhere than at 0
        self.RAM = data;
    }

    pub fn createRam(&mut self, addr: u16, length: uint) {
        assert!(addr == 0); // Unimplected: RAM elsewhere than at 0
        self.RAM = vec::with_capacity(length);
    }

    fn randRam(&mut self, addr: u16, mut length: uint) {
        let mut rng = rand::rng();
        if length == 0 { length = self.RAM.len() - addr as uint; }
        for i in range(0, length) {
            self.RAM[addr as uint + i] = rng.gen();
        }
    }


    fn error(&self, msg: ~str) {
        fail!("ERROR [{:s}:{:04X}]: {:s}", name(self.partNumber), self.PC as uint, msg);
        fail!("ERROR, I said");
    }

    fn warning(&self, msg: ~str) {
        info!("WARNING [{:s}:{:4X}]: {:s}", name(self.partNumber), self.PC as uint, msg);
    }

    fn fetch8(&self, address: u16, noerror: bool, norom: bool) -> u8 {
        // if (!noerror && self.READ_HOOKS.has_key(address)) {
        //     self.READ_HOOKS[address](address);
        // }
        if !norom {
            for (offset, rom) in self.ROM.iter() {
                let delta = address as int - *offset as int;
                if 0 <= delta && (delta as uint) < rom.len() {
                    return rom[delta];
                }
            }
        }

        if (address as uint) < self.RAM.len() {
            return self.RAM[address];
        }

        for i in range(0, self.PIAS.len()) {
            let pia = self.PIAS[i];
            let offset = address - unsafe { (*pia).address };
            if offset < 4 {
                return unsafe { (*pia).read(offset) };
            }
        }

        if address >= self.IOstart && address < self.IOstart + self.IOlength {
            return *self.IO.get(&address);
        }

        if !noerror {
            self.warning(format!("Read from invalid address ${:04X}", address));
        }

        0
    }

    fn fetch16(&self, address: u16, noerr: bool) -> u16 {
        return self.fetch8(address, noerr, false) as u16 * 256 +
            self.fetch8(address + 1, noerr, false) as u16;
    }

    pub fn fetchRam(&self, address: u16) -> u8 { self.RAM[address] }

    fn nextByte(&mut self) -> u8 { self.PC += 1; self.fetch8(self.PC - 1, false, false) }

    fn nextWord(&mut self) -> u16 { self.PC += 2; self.fetch16(self.PC - 2, false) }

    // Some friendlier accessors. Nomenclature is not that great.
    pub fn store(&mut self, dest:u16, value:u8) {
        self.store8(Address(dest), value as int, noflags);
    }
    pub fn storew(&mut self, dest:u16, value:u16) {
        self.store16(Address(dest), value as int, noflags);
    }
    pub fn fetch(&self, addr:u16) -> u8 { self.fetch8(addr, false, false) }
    pub fn fetchw(&self, addr:u16) -> u16 { self.fetch16(addr, false) }

    fn store8(&mut self, dest: Dest, value: int, flags: FlagSet) {
        let uvalue = value as u8;
        match dest {
            Reg(reg) => {
                match reg {
                    CC => self.set_CC(uvalue),
                    A  => self.A = uvalue,
                    B  => self.B = uvalue,
                    DP => self.DP = uvalue,
                    _  => self.error("Invalid store8 destination " + reg.name())
                }
            },
            Address(addr) => {
                if (addr as uint) < self.RAM.len() {
                    self.RAM[addr] = uvalue;
                } else {
                    let mut written = false;
                    for pia in self.PIAS.iter() {
                        let offset = addr - unsafe { (**pia).address };
                        if offset < 4 {
                            unsafe { (**pia).write(offset, uvalue) };
                            written = true;
                            break;
                        }
                    }
                    if !written {
                        if addr >= self.IOstart &&
                            addr < self.IOstart + self.IOlength {
                            self.IO.insert(addr, uvalue);
                        } else {
                            self.warning("Invalid store to address $" + x16(addr));
                        }
                    }
                }
                // TODO
                // if self.WRITE_HOOKS.has_key(addr) {
                //     self.WRITE_HOOKS[addr](uvalue, addr);
                // }
            }
            RegPair(_,_) => fail!("Impossible is nothing")
        }
        match flags {
            noflags => {}
            _ => self.tst8(flags, value)
        }
    }

    fn store16(&mut self, dest: Dest, value: int, flags: FlagSet) {
        let uvalue = value as u16;
        match dest {
            Address(addr) => {
                self.store8(dest, (value >> 8) as int, noflags);
                self.store8(Address(addr+1), value as u8 as int, noflags);
            },
            Reg(reg) => {
                match reg {
                    D  => self.set_D(uvalue),
                    X  => self.X = uvalue,
                    Y  => self.Y = uvalue,
                    S  => self.S = uvalue,
                    U  => self.U = uvalue,
                    PC => self.PC = uvalue,
                    _  => self.error("Invalid store16 destination " + reg.name())
                };
            }
            RegPair(_,_) => fail!("Impossible situation")
        }
        match flags {
            noflags => {}
            _ => self.tst16(flags, value)
        }
    }

    fn patch(&mut self, address: u16, value: u8) {
        for (offset, rom) in self.ROM.mut_iter() {
            let delta = address as uint - *offset as uint;
            if delta < rom.len() {
                return rom[delta] = value;
            }
        }
        self.error(~"No rom for patch");
    }

    fn tst8(&mut self, flagz: FlagSet, value: int) {
        let flags = actual(flagz);
        if flags.N {
            self.N = 1 == 1 & (value >> 7);
        }
        if flags.Z {
            self.Z = 0 == (value & 0xFF);
        }
        if flags.V {
            self.V = (value < -0x80 || value > 0x7F);
        }
        if flags.C {
            self.C = 1 == 1 & value >> 8;
        }
    }

    fn tst16(&mut self, flagz: FlagSet, value: int) {
        let flags = actual(flagz);
        if flags.N {
            self.N = 1 == 1 & (value >> 15);
        }
        if flags.Z {
            self.Z = 0 == (value & 0xFFFF);
        }
        if flags.V {
            self.V = value < -0x8000 || value > 0x7FFF;
        }
        if flags.C {
            self.C = 1 == 1 & value >> 16;
        }
    }


    fn reg(&self, r: Register) -> u16 {
        match r {
            CC => self.CC() as u16,
            A  => self.A as u16,
            B  => self.B as u16,
            D  => self.D(),
            DP => self.DP as u16,
            X  => self.X,
            Y  => self.Y,
            S  => self.S,
            U  => self.U,
            PC => self.PC,
            _    => { self.error("Invalid reg name " + r.name()); 0 }
        }
    }

    fn set_reg(&mut self, r: Register, v: u16) {
        match r {
            D  => self.set_D(v),
            X  => self.X = v,
            Y  => self.Y = v,
            S  => self.S = v,
            U  => self.U = v,
            PC => self.PC = v,
            _  => self.set_reg8(r, v as u8)
        };
    }

    fn set_reg8(&mut self, r: Register, v: u8) {
        match r {
            CC => self.set_CC(v),
            A  => self.A = v,
            B  => self.B = v,
            DP => self.DP = v,
            _  => self.error(format!("Invalid reg name '{:s}'", r.name()))
        };
    }

    fn push8(&mut self, value: u8, stack: Register) {
        let s = self.reg(stack) - 1;
        self.set_reg(stack, s);
        self.store8(Address(s + self.stackAdjust), value as int, noflags);
    }
    fn push16(&mut self, value: u16, stack: Register) {
        let s = self.reg(stack) - 2;
        self.set_reg(stack, s);
        self.store16(Address(s + self.stackAdjust), value as int, noflags);
    }

    fn pull8(&mut self, stack: Register) -> u8 {
        let s = self.reg(stack);
        self.set_reg(stack, s + 1);
        return self.fetch8(s + self.stackAdjust, false, false);
    }
    fn pull16(&mut self, stack: Register) -> u16 {
        let s = self.reg(stack);
        self.set_reg(stack, s + 2);
        return self.fetch16(s + self.stackAdjust, false);
    }

    // Unsigned 2"s complement adds
    fn add8(&mut self, v1: u8, v2: u8, c: u8, flagz: FlagSet) -> u8 {
        let sum = (v1 as int) + (v2 as int) + (c as int);
        let result = sum as u8;
        let flags = actual(flagz);
        if flags.N {
            self.N = 1 == 1 & (result >> 7);
        }
        if flags.Z {
            self.Z = result == 0;
        }
        if flags.V {
            self.V = 1 == 1 & ((v1 ^ v2 ^ result ^ (sum>>1) as u8) >> 7);
        }
        if flags.C {
            self.C = 1 == 1 & (sum >> 8);
        }
        return result;
    }

    fn add16(&mut self, v1: u16, v2: u16, c: u16, flagz: FlagSet) -> u16 {
        let sum = (v1 & 0xFFFF) + (v2 & 0xFFFF) + (c & 0xFFFF);
        let result = sum as u16;
        let flags = actual(flagz);
        if (flags.N) {
            self.N = 1 == 1 & (result >> 15);
        }
        if (flags.Z) {
            self.Z = result == 0;
        }
        if (flags.V) {
            self.V = 1 == 1 & ((v1 ^ v2 ^ sum ^ (sum>>1)) >> 15);
        }
        if (flags.C) {
            self.C = 1 == 1 & (sum >> 16);
        }
        return result;
    }

    fn sub8(&mut self, v1: u8, v2: u8, c: u8, flagz: FlagSet) -> u8 {
        let sum = (v1 as int) - (v2 as int) - (c as int);
        let result = sum as u8;
        let flags = actual(flagz);
        if flags.N {
            self.N = 1 == 1 & (result >> 7);
        }
        if flags.Z {
            self.Z = result == 0;
        }
        if flags.V {
            self.V = 1 == 1 & ((v1 ^ v2 ^ result ^ (sum>>1) as u8) >> 7);
        }
        if flags.C {
            self.C = 1 == 1 & (sum >> 8);
        }
        return result;
    }

    fn sub16(&mut self, v1: u16, v2: u16, c: u16, flagz: FlagSet) -> u16 {
        let sum = v1 - v2 - c;
        let result = sum as u16;
        let flags = actual(flagz);
        if flags.N {
            self.N = 1 == 1 & (result >> 15);
        }
        if flags.Z {
            self.Z = result == 0;
        }
        if flags.V {
            self.V = 1 == 1 & ((v1 ^ v2 ^ result ^ (sum>>1)) >> 15);
        }
        if flags.C {
            self.C = 1 == 1 & (sum >> 16);
        }
        return result;
    }

    fn tstH(&mut self, v1: u16, v2: u16, c: u8) {
        self.H = 1 == (((0xF & v1) + (0xF & v2) + c as u16) >> 4) & 1;
    }


    fn step6800(&mut self) -> bool {
        if (self.RESET) {
            self.reset(Reset);
            self.elapse(1); // ?
            return false;
        }
        self.s_initialized = self.s_initialized || self.S != UNINITIALIZED;
        if self.NMI && self.s_initialized {
            self.nmi6800();
            self.elapse(1); // ?
            return false;
        }
        if self.IRQ && !self.I {
            self.irq6800();
            self.elapse(1); // ?
            return false;
        }
        if self.WAIT {
            self.elapse(1);
            return false;
        }

        let opPC = self.PC;

        let opcode = self.nextByte();
        let (mnemonic, register, cycles, addrmode) = INSTRUCTIONS_6800(opcode).unwrap();

        // if !record {
            // self.error("Invalid opcode " + opcode + " @ " + self.PC);
        // }

        self.elapse(cycles);

        let width = register.bits();

        let addr = match addrmode {
            DIRECT     => Address(self.nextByte() as u16),
            EXTENDED   => Address(self.nextWord()),
            IMMEDIATE  => Address({ self.PC += 1; self.PC - 1 }),
            LIMMEDIATE => Address({ self.PC += 2; self.PC - 2 }),
            INDEXED    => Address(self.X + self.nextByte() as u16),
            RELATIVE   => Address(self.PC + (s8(self.nextByte()) as u16) + 1),
            INHERENT   => Reg(register),
            _          => { self.error(format!("Invalid addressing mode {}", addrmode as int)); Address(0) }
        };

        let R = self.reg(register);

        let store = |a,v,f| { if width == 16 { self.store16(a,v,f) } else { self.store8(a,v,f) } };
        let add = |a,b,c,f| { if width == 16 { self.add16(a,b,c,f) } else { self.add8(a as u8,b as u8,c as u8,f) as u16 } };
        let sub = |a,b,c,f| { if width == 16 { self.sub16(a,b,c,f) } else { self.sub8(a as u8,b as u8,c as u8,f) as u16 } };
        let tst = |f,a| { if width == 16 { self.tst16(f,a) } else { self.tst8(f,a) } };

        let fetch = || {  // call only once!
            //fetch = || { failure!("Call only once"); };
            match addr {
                Reg(_) => R,
                Address(addr) =>
                    if width == 16 {
                       self.fetch16(addr, false)
                    } else {
                       self.fetch8(addr, false, false) as u16
                    },
                RegPair(_,_) => fail!("Impossible dream")
            }
        };

        if mnemonic == "ABA" {
            self.tstH(self.A as u16, self.B as u16, 0);
            self.A = self.add8(self.A, self.B, 0, NZVC);

        } else if mnemonic == "ADC" {
            let v = fetch();
            self.tstH(R, v, bit(self.C));
            store(Reg(register), add(R, v, bit16(self.C), NZVC) as int, noflags);

        } else if mnemonic == "ADD" {
            let v = fetch();
            self.tstH(R, v, 0);
            store(Reg(register), add(R, v, 0, NZVC) as int, noflags);

        } else if mnemonic == "AND" {
            let flags = match register { CC => noflags, _ => NZV };
            store(Reg(register), (R & fetch()) as int, flags);

        } else if mnemonic == "ASL" {
            let v = fetch();
            store(addr, add(v, v, 0, NZVC) as int, noflags);

        } else if mnemonic == "ASR" {
            let v = fetch();
            store(addr, (v >> 1) as int, NZ);
            self.C = 1 == v & 1;
            self.V = self.N ^ self.C;

        } else if mnemonic == "BCC" {
            if !self.C { self.PC = addr.addr(); }
        } else if mnemonic == "BCS" {
            if self.C { self.PC = addr.addr(); }
        } else if mnemonic == "BEQ" {
            if self.Z { self.PC = addr.addr(); }
        } else if mnemonic == "BGE" {
            if self.N == self.V { self.PC = addr.addr(); }
        } else if mnemonic == "BGT" {
            if self.N == self.V && !self.Z { self.PC = addr.addr(); }
        } else if mnemonic == "BHI" {
            if !self.Z && !self.C { self.PC = addr.addr(); }
        } else if mnemonic == "BLE" {
            if self.N != self.V || self.Z { self.PC = addr.addr(); }
        } else if mnemonic == "BLS" {
            if self.Z || self.C { self.PC = addr.addr(); }
        } else if mnemonic == "BLT" {
            if self.N != self.V { self.PC = addr.addr(); }
        } else if mnemonic == "BMI" {
            if self.N { self.PC = addr.addr(); }
        } else if mnemonic == "BNE" {
            if !self.Z { self.PC = addr.addr(); }
        } else if mnemonic == "BPL" {
            if !self.N { self.PC = addr.addr(); }
        } else if mnemonic == "BRA" {
            self.PC = addr.addr();
        } else if mnemonic == "BSR" {
            self.push16(self.PC, S);
            self.PC = addr.addr();
        } else if mnemonic == "BVC" {
            if !self.V { self.PC = addr.addr(); }
        } else if mnemonic == "BVS" {
            if self.V { self.PC = addr.addr(); }

        } else if mnemonic == "BIT" {
            tst(NZV, (R & fetch()) as int);

        } else if mnemonic == "CBA" {
            self.sub8(self.A, self.B, 0, NZVC);

        } else if mnemonic == "CLC" {
            self.C = false;

        } else if mnemonic == "CLI" {
            self.I = false;

        } else if mnemonic == "CLR" {
            store(addr, 0, noflags);
            self.N = false;
            self.V = false;
            self.C = false;
            self.Z = true;

        } else if mnemonic == "CLV" {
            self.V = false;

        } else if mnemonic == "CMP" || mnemonic == "CP" {
            sub(R, fetch(), 0, NZVC);

        } else if (mnemonic == "COM") {
            store(addr, !fetch() as int, NZ);
            self.V = false;
            self.C = true;

        } else if (mnemonic == "DAA") {
            let ahi = (self.A >> 4) & 0xF;
            let alo = self.A & 0xF;
            if (self.C || ahi > 9 || (ahi > 8 && alo > 9)) {
                self.A += 0x60;
                self.C = true;
            }
            if self.H || alo > 9 {
                self.A += 6;
            }
            self.A &= 0xFF;
            tst(NZ, self.A as int);

        } else if (mnemonic == "DEC") {
            store(addr, sub(fetch(), 1, 0, NZV) as int, noflags);

        } else if (mnemonic == "DE") {
            let f = match register { X => Z, _ => noflags };
            store(addr, sub(fetch(), 1, 0, f) as int, noflags);

        } else if (mnemonic == "EOR") {
            store(Reg(register), (R ^ fetch()) as int, NZV);

        } else if (mnemonic == "INC") {
            store(addr, add(fetch(), 1, 0, NZV) as int, noflags);

        } else if (mnemonic == "IN") {
            let f = match register { X => Z, _ => noflags };
            store(addr, add(fetch(), 1, 0, f) as int, noflags);

        } else if (mnemonic == "JMP") {
            self.PC = addr.addr();

        } else if (mnemonic == "JSR") {
            self.push16(self.PC, S);
            self.PC = addr.addr();

        } else if (mnemonic == "LDA" || mnemonic == "LD") {
            store(Reg(register), fetch() as int, NZ);
            self.V = false;

        } else if (mnemonic == "LSR") {
            let v = fetch();
            store(addr, (0x7F & (v >> 1)) as int, NZ);
            self.C = 1 == v & 1;
            self.V = self.N ^ self.Z;

        } else if (mnemonic == "NEG") {
            let n = -(fetch() as int);
            store(addr, n, NZ);
            assert!(width == 8); // seems to be the assumption below
            self.V = (n as u8) == 0x80;
            self.C = !self.Z;

        } else if (mnemonic == "NOP") {
            ;

        } else if (mnemonic == "ORA") {
            store(Reg(register), (R | fetch()) as int, NZ);
            self.V = false;

        } else if (mnemonic == "PSH") {
            assert!(width == 8);
            self.push8(R as u8, S);

        } else if (mnemonic == "PUL") {
            let s = self.pull8(S);
            self.set_reg8(register, s);

        } else if (mnemonic == "ROL") {
            store(addr, ((fetch() as u8 << 1) | bit(self.C)) as int, NZC);
            self.V = self.N ^ self.C;

        } else if (mnemonic == "ROR") {
            let v = fetch();
            let msb = bit(self.C) << 7;
            store(addr, (msb | (v as u8 >> 1)) as int, NZ);
            self.C = 1 == v & 1;
            self.V = self.N ^ self.C;

        } else if (mnemonic == "RTI") {
            let cc = self.pull8(S);
            self.set_CC(cc);
            self.B = self.pull8(S);
            self.A = self.pull8(S);
            self.X = self.pull16(S);
            self.PC = self.pull16(S);

        } else if (mnemonic == "RTS") {
            self.PC = self.pull16(S)

        } else if (mnemonic == "SBA") {
            self.A = self.sub8(self.A, self.B, 0, NZVC);

        } else if (mnemonic == "SBC") {
            store(Reg(register), sub(R, fetch(), bit16(self.C), NZVC) as int, noflags);

        } else if (mnemonic == "SEC") {
            self.C = true;

        } else if (mnemonic == "SEI") {
            self.I = true;

        } else if (mnemonic == "SEV") {
            self.V = true;

        } else if (mnemonic == "STA" || mnemonic == "ST") {
            store(addr, R as int, NZ);
            self.V = false;

        } else if (mnemonic == "SUB") {
            store(Reg(register), sub(R, fetch(), 0, NZVC) as int, noflags);

        } else if (mnemonic == "SWI") {
            self.push16(self.PC, S);
            self.push16(self.X, S);
            self.push8(self.A, S);
            self.push8(self.B, S);
            let cc = self.CC();
            self.push8(cc, S);
            if (mnemonic == "SWI") { // ?
                self.I = true;
                self.F = true;
            }
            self.PC = self.fetch16(0xFFFA, false);

        } else if (mnemonic == "TAB") {
            self.B = self.A;
            tst(NZ, self.B as int);
            self.V = false;

        } else if (mnemonic == "TBA") {
            self.A = self.B;
            tst(NZ, self.A as int);
            self.V = false;

        } else if (mnemonic == "TAP") {
            self.set_CC(self.A);

        } else if (mnemonic == "TPA") {
            self.A = self.CC();

        } else if (mnemonic == "TST") {
            self.V = false;
            self.C = false;
            tst(NZ, fetch() as int);

        } else if (mnemonic == "TSX") {
            self.X = self.S;

        } else if (mnemonic == "TXS") {
            self.S = self.X;

        } else if (mnemonic == "WAI") {
            self.push16(self.PC, S);
            self.push16(self.X, S);
            self.push8(self.A, S);
            self.push8(self.B, S);
            let cc = self.CC();
            self.push8(cc, S);
            self.WAIT = true;
            return false;

        } else {
            self.error("Invalid mnemonic " + mnemonic);
            return false;
        }

        // If in a tight busy loop, we can save ourselves some cycles.
        // There"s no single instruction that changes anything and loops,
        // so we"re just waiting for an interrupt.
        if self.PC == opPC {
            self.WAIT = true;
        }

        return true;
    }


    fn step6809(&mut self) -> bool {
        if (self.RESET) {
            self.reset(Reset);
            self.elapse(1);
            return false;
        }
        if (self.NMI) {
            self.s_initialized = self.s_initialized || self.S != UNINITIALIZED;
            if self.s_initialized {
                self.nmi6809();
                self.elapse(1);
                return false;
            } else if (self.SYNC) {
                self.SYNC = false;
                self.WAIT = false;
            }
        }
        if (self.FIRQ) {
            if !self.F {
                self.firq();
                self.elapse(1);
                return false;
            } else if (self.SYNC) {
                self.SYNC = false;
                self.WAIT = false;
            }
        }
        if (self.IRQ) {
            if !self.I {
                self.irq6809();
                self.elapse(1);
                return false;
            } else if self.SYNC {
                self.SYNC = false;
                self.WAIT = false;
            }
        }

        if (self.WAIT || self.SYNC) {
            self.elapse(1);
            return false;
        }

        let mut opcode = self.nextByte() as u16;
        if opcode == 0x10 || opcode == 0x11 {
            opcode = opcode * 256 + self.nextByte() as u16;
        }
        let (mnemonic, register, mode, ti, _, legality) = INSTRUCTIONS_6809(opcode);
        let mut timing = ti;

        // if !record {
        //     self.error(format!("Unrecognised opcode: {:02X}", opcode));
        // }
        let mut autoincrement = 0;

        let width = register.bits();

        match legality { FALLBACK => { timing += 1; }, _ => {} }

        self.elapse(timing);

        let addr = match mode {
            EXTENDED => {
                Address(self.nextWord())
            }
            INHERENT => {
                Reg(register)
            }
            RELATIVE => {
                let r = self.nextByte() as i8;
                Address(self.PC + r as u16)
            }
            LRELATIVE => {
                let r = self.nextWord() as i16;
                Address(self.PC + r as u16)
            }
            DIRECT => {
                Address(self.DP as u16 * 256 + self.nextByte() as u16)
            }
            REGISTER => {
                let TFR_REGS = [D, X, Y, U, S, PC, o, o,
                                A, B, CC, DP];
                let postbyte = self.nextByte();
                RegPair(TFR_REGS[postbyte & 0xF], TFR_REGS[postbyte >> 4])
            }
            LIMMEDIATE => fail!("Impossible"),
            IMMEDIATE => {
                let addr = self.PC;
                self.PC += width as u16 / 8;
                Address(addr)
            }
            INDEXED => {
                let postbyte = self.nextByte();
                let subregister = [X,Y,U,S][(postbyte>>5) & 0x3];
                let indirect = ((postbyte & 0x10) != 0);
                let mut submode = postbyte & 0xF;
                let addr = if postbyte == 0x9F {
                    // Extended indirect
                    self.elapse(5);
                    let ptr = self.nextWord();
                    self.fetch16(ptr, false)
                } else if ((postbyte & 0x80) == 0) {
                    // 5-bit offset from register contained in postbyte
                    self.elapse(1);
                    if indirect { submode -= 0x10; }
                    self.reg(subregister) + submode as u16
                } else {
                    let mut addr = self.reg(subregister);
                    let offset = if (submode == 0x4) {
                        // No offset from register
                        0
                    } else if submode == 0x8 {
                        // 8-bit offset from register
                        self.elapse(1);
                        s8(self.nextByte())
                    } else if submode == 0x9 {
                        // 16-bit offset from register
                        self.elapse(4);
                        s16(self.nextWord())
                    } else if submode == 0x6 {
                        // A-register offset from register
                        self.elapse(1);
                        self.A as int
                    } else if submode == 0x5 {
                        // B-register offset from register
                        self.elapse(1);
                        self.B as int
                    } else if submode == 0xB {
                        // D-register offset from register
                        self.elapse(4);
                        self.D() as int
                    } else if submode == 0 {
                        // Post-increment by 1
                        self.elapse(2);
                        autoincrement = 1;
                        0
                    } else if submode == 1 {
                        // Post-increment by 2
                        self.elapse(3);
                        autoincrement = 2;
                        0
                    } else if submode == 2 {
                        // Pre-decrement by 1
                        self.elapse(2);
                        autoincrement = -1;
                        -1    // it"s a pre-decrement
                    } else if submode == 3 {
                        // Pre-decrement by 2
                        self.elapse(3);
                        autoincrement = -2;
                        -2    // it"s a pre-decrement
                    } else if submode == 0xC {
                        // 8-bit offset from PC
                        self.elapse(1);
                        addr = 0;
                        s8(self.nextByte())
                    } else if submode == 0xD {
                        // 16-bit offset from PC
                        self.elapse(5);
                        addr = 0;
                        s16(self.nextWord())
                    } else {
                        self.error(format!("Invalid indexing submode {:02X} in postbyte {:02X}", submode as uint, postbyte as uint));
                        0
                    };

                    addr += offset as u16;

                    if indirect {
                        self.elapse(3);
                        self.fetch16(addr, false)
                    } else {
                        addr
                    }
                };

                if autoincrement != 0 {
                    let sr = self.reg(subregister);
                    self.set_reg(subregister, sr + autoincrement);
                }

                Address(addr)
            }
        };

        let store = |a,v,f| { if width == 16 { self.store16(a,v,f) } else { self.store8(a,v,f) } };
        let add = |a,b,c,f| { if width == 16 { self.add16(a,b,c,f) } else { self.add8(a as u8,b as u8,c as u8,f) as u16 } };
        let sub = |a,b,c,f| { if width == 16 { self.sub16(a,b,c,f) } else { self.sub8(a as u8,b as u8,c as u8,f) as u16 } };
        let tst = |f,a| { if width == 16 { self.tst16(f,a) } else { self.tst8(f,a) } };

        let R = match register { o => 0, _ => self.reg(register) };

        let fetch = || {  // call only once!
            //fetch = || { failure!("Call only once"); };
            match addr {
                Reg(_) => R,
                Address(addr) =>
                    if width == 16 {
                        self.fetch16(addr, false)
                    } else {
                        self.fetch8(addr, false, false) as u16
                    },
                RegPair(_,_) => fail!("Impossible")
            }
        };

        if mnemonic == "ABX" {
            self.X = self.X + self.B as u16;

        } else if mnemonic == "ADC" {
            let v = fetch();
            self.tstH(R, v, bit(self.C));
            store(Reg(register), add(R, v, bit16(self.C), NZVC) as int, noflags);

        } else if mnemonic == "ADD" {
            let v = fetch();
            self.tstH(R, v, 0);
            store(Reg(register), add(R, v, 0, NZVC) as int, noflags);

        } else if mnemonic == "AND" {
            let f = match register { CC => noflags, _ => NZV };
            store(Reg(register), (R & fetch()) as int, f);

        } else if mnemonic == "ASL" {
            store(addr, (fetch() << 1) as int, NZC);
            self.V = self.N ^ self.C;

        } else if mnemonic == "ASR" {
            let v = fetch();
            store(addr, (v >> 1) as int, NZ);
            self.C = 1 == v & 1;

        } else if mnemonic.starts_with("BCC") {
            if !self.C { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BCS") {
            if self.C { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BEQ") {
            if self.Z { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BGE") {
            if self.N == self.V { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BGT") {
            if self.N == self.V && !self.Z { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BHI") {
            if !self.Z && !self.C { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BLE") {
            if self.N != self.V || self.Z { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BLS") {
            if self.Z || self.C { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BLT") {
            if self.N != self.V { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BMI") {
            if self.N { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BNE") {
            if !self.Z { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BPL") {
            if !self.N { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BRA") {
            self.PC = addr.addr();
        } else if mnemonic.starts_with("BRN") {
            ;
        } else if mnemonic.starts_with("BSR") {
            self.push16(self.PC, S);
            self.PC = addr.addr();
        } else if mnemonic.starts_with("BVC") {
            if !self.V { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }
        } else if mnemonic.starts_with("BVS") {
            if self.V { self.PC = addr.addr(); if mnemonic.starts_with("L") {self.elapse(1);} }

        } else if mnemonic == "BIT" {
            tst(NZV, (R & fetch()) as int);

        } else if mnemonic == "CLR" {
            store(addr, 0, NZVC);

        } else if mnemonic == "CMP" {
            sub(R, fetch(), 0, NZVC);

        } else if mnemonic == "COM" {
            store(addr, (!fetch()) as int, NZV);
            self.C = true;

        } else if mnemonic == "CWAI" {
            let cc = self.CC() & self.fetch8(addr.addr(), false, false);
            self.set_CC(cc as u8);
            self.E = true;
            self.push16(self.PC, S);
            self.push16(self.U, S);
            self.push16(self.Y, S);
            self.push16(self.X, S);
            self.push8(self.DP, S);
            self.push8(self.B, S);
            self.push8(self.A, S);
            self.push8(cc, S);
            self.WAIT = true;

        } else if mnemonic == "DAA" {
            let ahi = (self.A >> 4) & 0xF;
            let alo = self.A & 0xF;
            if (self.C || ahi > 9 || (ahi > 8 && alo > 9)) {
                self.A += 0x60;
                self.C = true;
            }
            if (self.H || alo > 9) {
                self.A += 6;
            }
            self.A &= 0xFF;
            tst(NZ, self.A as int);

        } else if mnemonic == "DEC" {
            store(addr, sub(fetch(), 1, 0, NZV) as int, noflags);

        } else if mnemonic == "EOR" {
            store(Reg(register), (R ^ fetch()) as int, NZV);

        } else if mnemonic == "EXG" {
            match addr {
                RegPair(r1, r2) => {
                    let v1 = match r1 {
                        o => -1,
                        _ => {
                            let tmp = self.reg(r1);
                            match r2 {
                                o => {}
                                _ => {
                                    let v2 = self.reg(r2);
                                    self.set_reg(r1, v2);
                                }
                            }
                            tmp
                        }
                    };
                    match r2 {
                        o => {}
                        _ => { self.set_reg(r2, v1); }
                    }
                }
                _ => fail!("Impossible")
            }

        } else if mnemonic == "INC" {
            store(addr, add(fetch(), 1, 0, NZV) as int, noflags);

        } else if mnemonic == "JMP" {
            self.PC = addr.addr();

        } else if mnemonic == "JSR" {
            self.push16(self.PC, S);
            self.PC = addr.addr();

        } else if mnemonic == "LD" {
            store(Reg(register), fetch() as int, NZV);

        } else if mnemonic == "LEA" {
            store(Reg(register), addr.addr() as int, noflags);
            match register { X | Y => tst(Z, addr.addr() as int), _ => fail!("Impossible") }

        } else if mnemonic == "LSR" {
            let v = fetch();
            store(addr, (v >> 1) as int, NZ);
            self.C = 1 == v & 1;

        } else if mnemonic == "MUL" {
            self.set_D(self.A as u16 * self.B as u16);
            self.Z = self.D() != 0;
            self.C = 1 == 1 & (self.B >> 7);

        } else if mnemonic == "NEG" {
            let v = fetch();
            self.C = v != 0;
            store(addr, sub(0, v, 0, NZV) as int, noflags);

        } else if mnemonic == "NOP" {
            ;

        } else if mnemonic == "!HCF" {
            self.error(~"Halting and catching fire as instructed.");

        } else if mnemonic == "OR" {
            let f = match register { CC => noflags, _ => NZV };
            store(Reg(register), (R | fetch()) as int, f);

        } else if mnemonic.starts_with("PSH") {
            let stack = if mnemonic.ends_with("S") {S} else {U};
            let mask = fetch();
            if mask & 0x80 != 0 { self.elapse(2); self.push16(self.PC, stack); }
            if mask & 0x40 != 0 { let other = if mnemonic.ends_with("S") {self.U} else {self.S};
                                  self.elapse(2); self.push16(other,  stack); }
            if mask & 0x20 != 0 { self.elapse(2); self.push16(self.Y, stack); }
            if mask & 0x10 != 0 { self.elapse(2); self.push16(self.X, stack); }
            if mask & 0x08 != 0 { self.elapse(1); self.push8(self.DP, stack); }
            if mask & 0x04 != 0 { self.elapse(1); self.push8(self.B, stack); }
            if mask & 0x02 != 0 { self.elapse(1); self.push8(self.A, stack); }
            if mask & 0x01 != 0 { let cc = self.CC();
                                  self.elapse(1); self.push8(cc, stack); }

        } else if mnemonic.starts_with("PUL") {
            let stack = if mnemonic.ends_with("S") {S} else {U};
            let mask = fetch();
            if mask & 0x01 != 0 { let self_pull8_stack_     = self.pull8(stack);
                                  self.elapse(1); self.set_CC(self_pull8_stack_); }
            if mask & 0x02 != 0 { self.elapse(1); self.A    = self.pull8(stack); }
            if mask & 0x04 != 0 { self.elapse(1); self.B    = self.pull8(stack); }
            if mask & 0x08 != 0 { self.elapse(1); self.DP   = self.pull8(stack); }
            if mask & 0x10 != 0 { self.elapse(2); self.X    = self.pull16(stack); }
            if mask & 0x20 != 0 { self.elapse(2); self.Y    = self.pull16(stack); }
            if mask & 0x40 != 0 { self.elapse(2); let other = self.pull16(stack);
                                  match stack { S => self.U = other, _ => self.S = other } }
            if mask & 0x80 != 0 { self.elapse(2); self.PC = self.pull16(stack); }

        } else if mnemonic == "ROL" {
            store(addr, ((fetch() << 1) | bit16(self.C)) as int, NZVC);

        } else if mnemonic == "ROR" {
            let v = fetch() as u8;
            let msb = bit(self.C) << 7;
            self.C = 1 == v & 1;
            store(addr, (msb | (v >> 1)) as int, NZ);

        } else if mnemonic == "RTI" {
            let cc = self.pull8(S);
            self.set_CC(cc);
            if (self.E) {
                self.A = self.pull8(S);
                self.B = self.pull8(S);
                self.DP = self.pull8(S);
                self.X = self.pull16(S);
                self.Y = self.pull16(S);
                self.U = self.pull16(S);
                self.elapse(9);
            }
            self.PC = self.pull16(S);

        } else if mnemonic == "RTS" {
            self.PC = self.pull16(S)

        } else if mnemonic == "SBC" {
            store(Reg(register), sub(R, fetch(), bit16(self.C), NZVC) as int, noflags);

        } else if mnemonic == "SEX" {
            self.tst8(NZ, self.B as int);
            self.set_D(self.B as i8 as u16);

        } else if mnemonic == "ST" {
            store(addr, R as int, NZV);

        } else if mnemonic == "SUB" {
            store(Reg(register), sub(R, fetch(), 0, NZVC) as int, noflags);

        } else if mnemonic.starts_with("SWI") || mnemonic == "RESET" {
            self.E = true;
            self.push16(self.PC, S);
            self.push16(self.U, S);
            self.push16(self.Y, S);
            self.push16(self.X, S);
            self.push8(self.DP, S);
            self.push8(self.B, S);
            self.push8(self.A, S);
            let cc = self.CC();
            self.push8(cc, S);
            if mnemonic == "SWI" {
                self.I = true;
                self.F = true;
            }
            self.PC = self.fetch16(match mnemonic {
                "SWI"   => 0xFFFA,
                "SWI2"  => 0xFFF4,
                "SWI3"  => 0xFFF2,
                "RESET" => 0xFFFE,
                _       => fail!("Impossible")}, false);

        } else if mnemonic == "SYNC" {
            self.SYNC = true;

        } else if mnemonic == "TFR" {
            match addr {
                RegPair(o, _) => {}
                RegPair(r1, o) => { self.set_reg(r1, -1); }
                RegPair(r1, r2) => { let v2 = self.reg(r2); self.set_reg(r1, v2); }
                _ => fail!("Impossible")
            }

        } else if mnemonic == "TST" {
            tst(NZV, fetch() as int);

        } else {
            self.error(format!("Unimplemented opcode {:02X} / {:s}", opcode as uint, mnemonic));
        }

        return true;
    }

    fn reset(&mut self, target:ResetTarget) {
        self.FIRQ = false;
        self.IRQ = false;
        self.NMI = false;
        self.RESET = false;
        self.S = 0;
        self.WAIT = false;
        self.SYNC = false;
        let addr = match target {
            Reset => self.fetch16(0xFFFE, false),
            Vector(addr) => addr
        };
        self.PC = addr;
    }

    fn nmi6800(&mut self) {
        self.NMI = false;
        self.WAIT = false;
        self.push16(self.PC, S);
        self.push16(self.X, S);
        self.push8(self.A, S);
        self.push8(self.B, S);
        let cc = self.CC();
        self.push8(cc, S);
        self.I = true;
        self.PC = self.fetch16(0xFFFC, false);
    }

    fn nmi6809(&mut self) {
        self.NMI = false;
        self.WAIT = false;
        self.SYNC = false;
        let addr = self.fetch16(0xFFFC, false);
        self.E = true;
        self.push16(self.PC, S);
        self.push16(self.U, S);
        self.push16(self.Y, S);
        self.push16(self.X, S);
        self.push8(self.DP, S);
        self.push8(self.B, S);
        self.push8(self.A, S);
        let cc = self.CC();
        self.push8(cc, S);
        self.F = true;
        self.I = true;
        self.PC = addr;
    }

    fn firq(&mut self) {
        self.FIRQ = false;
        self.WAIT = false;
        self.SYNC = false;
        self.E = false;
        self.push16(self.PC, S);
        let cc = self.CC();
        self.push8(cc, S);
        self.F = true;
        self.I = true;
        self.PC = self.fetch16(0xFFF6, false);
    }

    fn irq6800(&mut self) {
        self.IRQ = false;
        self.WAIT = false;
        self.push16(self.PC, S);
        self.push16(self.X, S);
        self.push8(self.A, S);
        self.push8(self.B, S);
        let cc = self.CC();
        self.push8(cc, S);
        self.I = true;
        self.PC = self.fetch16(0xFFF8, false);
    }

    fn irq6809(&mut self) {
        self.IRQ = false;
        self.WAIT = false;
        self.SYNC = false;
        self.E = true;
        self.push16(self.PC, S);
        self.push16(self.U, S);
        self.push16(self.Y, S);
        self.push16(self.X, S);
        self.push8(self.DP, S);
        self.push8(self.B, S);
        self.push8(self.A, S);
        let cc = self.CC();
        self.push8(cc, S);
        self.I = true;
        self.PC = self.fetch16(0xFFF8, false);
    }

    fn showram(&self, andzero: bool) {
        println("RAM:");
        for i in range(0, self.RAM.len()) {
            let v = self.RAM[i];
            if andzero || v != 0 {
                info!("    {:04X}: {:02X} {:4d}", i, v, v as i8);
            }
        }
    }

    fn vectors(&self) -> hashmap::HashMap<&str, u16> {
        let mut result: hashmap::HashMap<&str, u16> = hashmap::HashMap::new();
        result.insert("reset", self.fetch16(0xFFFE, false));
        result.insert("nmi", self.fetch16(0xFFFC, false));
        result.insert("swi", self.fetch16(0xFFFA, false));
        result.insert("irq", self.fetch16(0xFFF8, false));
        match self.partNumber { MC6809 => {
            result.insert("firq", self.fetch16(0xFFF6, false));
            result.insert("swi2", self.fetch16(0xFFF4, false));
            result.insert("swi3", self.fetch16(0xFFF2, false));
        }, _ => {}}
        result
    }

    fn run(&mut self, limit: f64) {
        let initial = self.clock();
        while !self.WAIT && !self.SYNC && self.clock() - initial < limit {
            let pc = self.PC;
            if self.verbose {
                self.trace();
            }
            if !self.step() {
                break;
            }
            if pc == self.PC {
                self.error(~"INFINITE LOOP");
                break;
            }
        }
    }

    fn elapse(&mut self, cycles: int) {
        self.CLOCK += cycles;
        if (self.CLOCK > 1000000) {
            self.MCLOCK += 1;
            self.CLOCK -= 1000000;
        }
    }

    pub fn clock(&self) -> f64 { (self.MCLOCK as f64 + self.CLOCK as f64 / 1000000.0) / self.clockRate }

    pub fn hookWrite(&mut self, _addr:u16, _callback: &|u8, u16|) {
        // self.WRITE_HOOKS.insert(addr, callback);
    }

    // pub fn hookRead(&mut self, addr:u16, callback: & fn(u16)) {
    //     self.READ_HOOKS.insert(addr, callback);
    // }

    pub fn connectPia(&mut self, pia:&mut Pia6821, addr: u16) {
        (*pia).mpu = ptr::to_mut_unsafe_ptr(self);
        (*pia).address = addr;
        self.PIAS.push(ptr::to_mut_unsafe_ptr(pia));
    }

    pub fn step(&mut self) -> bool {
        match self.partNumber {
            MC6809                   => self.step6809(),
            MC6800 | MC6802 | MC6808 => self.step6800()
        }
    }

    pub fn disassemble(&self, pc:u16) -> ~Disassembly {
        match self.partNumber {
            MC6809                   => self.disassemble6809(pc),
            MC6800 | MC6802 | MC6808 => self.disassemble6800(pc)
        }
    }

    fn trace(&self) {
        let d = self.disassemble(self.PC);
        println(format!("{:f}  {:s}", self.clock(), d.short));
    }

}


struct Channel<'a> {
    CR: u8,
    P: u8,
    DDR: u8,
    OUTPUT: ~|u8|,
}

struct Pia6821 {
    channel: ~[Channel, ..2],
    address: u16,
    mpu: *mut Cpu
}

fn nothing() {}

impl Pia6821 {

    pub fn new() -> Pia6821 {
        Pia6821 {
            channel: [Channel { CR:0, P:0, DDR:0, OUTPUT: |_|{} },
                      Channel { CR:0, P:0, DDR:0, OUTPUT: |_|{} }],
            address: 0,
            mpu: ptr::mut_null(),
        }
    }

    fn reset(&mut self) {
        self.channel[0].CR = 0;
        self.channel[1].CR = 0;
        self.channel[0].P = 0;
        self.channel[1].P = 0;
        self.channel[0].DDR = 0;
        self.channel[1].DDR = 0;
    }

    fn interruptEnabledOnIndex(&self, index: int) -> bool {
        let cr = self.channel[index].CR;
        cr & 0x1 != 0 || (!(cr & 0x20 != 0) && (cr & 0x08 != 0))
    }

    pub fn connectPeripheralOutput(&mut self, index: int, callback: ~|u8|) {
        self.channel[index].OUTPUT = callback
    }

    pub fn peripheralInput(&mut self, index: int, value:u8, mut mask:u8) {
        //if undef(mask) { mask = ~self.channel[index].DDR; }
        mask &= !self.channel[index].DDR;
        self.channel[index].P = (value & mask) | (self.channel[index].P & !mask);
        if (self.interruptEnabledOnIndex(index)) {
            unsafe {
                (*self.mpu).IRQ = true;
            }
        }
    }

    fn write(&mut self, offset: u16, value: u8) {
        let tipe = offset & 1;
        let index = offset >> 1;
        if (tipe == 0) {
            if self.channel[index].CR & 0x4 != 0 {
                self.channel[index].P = (value & self.channel[index].DDR) |
                    (self.channel[index].P & !self.channel[index].DDR);
                (self.channel[index].OUTPUT)(self.channel[index].P);
            } else {
                self.channel[index].DDR = value;
            }
        } else {
            self.channel[index].CR = value;
        }
    }

    fn read(&mut self, offset: u16) -> u8 {
        let tipe = offset & 1;
        let index = (offset >> 1) as int;
        if (tipe == 0) {
            if (self.channel[index].CR & 0x4 != 0) {
                return self.channel[index].P;
            } else {
                return self.channel[index].DDR;
            }
        } else {
            let result = self.channel[index].CR;
            if (self.channel[index].CR & 0xC0 != 0) {
                if (self.interruptEnabledOnIndex(index)) {
                    unsafe {
                        (*self.mpu).IRQ = false;
                    }
                }
                self.channel[index].CR &= 0x3F;    // clear interrupt status bits
            }
            return result;
        }
    }

    fn toString(&self) -> ~str {
        return format!("{:4X}: A[{:s}] {:02X} {:02X} / B{:s} {:02X} {:02X}",
                    self.address as uint,
                    if self.channel[0].DDR == 0 {
                        ~"ii"
                    } else if self.channel[0].DDR == 0xFF {
                          ~"oo"
                    } else {
                         x8(self.channel[0].DDR)
                    },
                    self.channel[0].P as uint,
                    self.channel[0].CR as uint,
                    if self.channel[1].DDR == 0 {
                        ~"ii"
                    } else if self.channel[1].DDR == 0xFF {
                        ~"oo"
                    } else {
                        x8(self.channel[1].DDR)
                    },
                    self.channel[1].P as uint,
                    self.channel[1].CR as uint);
    }
}

fn main() {
    let mut main = Cpu::new(MC6809, 1.0);

    //main.createRam(0, 0xD000);
    main.loadRom(0xF000, read_file(~"build/test.bin"));
    main.reset(Vector(0xF000));
    main.verbose = true;
    main.run(0.05);
    //main.showram();
}